module.exports = {
  collectCoverageFrom: [
    'components/**/*.{js,jsx}',
    '!components/styles/**/*.{js,jsx}',
    'store/**/*.js',
    'routes/**/*.js',
  ],
  setupFiles: ['<rootDir>/jest.setup.js'],
  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
};
