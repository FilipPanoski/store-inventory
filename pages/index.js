import React from 'react';
import { connect } from 'react-redux';

import { FormattedMessage } from 'react-intl';
import DialogTypes from '../utils/DialogTypes';
import { toggleDialog } from '../store/actions/dialog';
import { Card, CardBody, CardHeader } from '../components/styles/Card';
import { startSession } from '../store/actions/storeSession';
import { ITEM_NOT_FOUND, ERROR_ON_LOCATION } from '../utils/inventoryItemStatuses';
import { SERIALIZED } from '../utils/inventoryItemTypes';
import ModalStyles, { Container } from '../components/styles/Modal/ModalStyles';

import TableList from '../components/TableList';
import Head from '../components/Head';
import SessionToolbar from '../components/SessionToolbar';
import DialogContainer from '../components/DialogContainer';
import Button from '../components/styles/Button/Button';
import ActionButtonGroup from '../components/styles/ActionButtonGroup/ActionButtonGroup';
import { ErrorMessage } from '../components/styles/ErrorMessage';
import { SUBMITTING } from '../utils/submitSessionStatus';
import Loader from '../components/Loader';
import { submitToErpStatus } from '../store/actions/erpSubmit';
import IndexUtils from '../utils/indexUtils';

class Home extends React.Component {
  static async getInitialProps(props) {
    const { isServer, req, store } = props;

    if (isServer && req) {
      const { user } = req.session;
      const isLoading = false;

      store.dispatch(
        startSession(req.session.store, user, req.headers.cookie, isLoading),
      );
    }

    return {};
  }

  handleItemClick = (item) => {
    switch (item.statusType) {
      case ITEM_NOT_FOUND:
        this.handleItemNotFound(IndexUtils.generateNotFoundErrorItem(item));
        break;
      case ERROR_ON_LOCATION:
        if (item.inventoryItemType === SERIALIZED) {
          this.handleLocationError(IndexUtils.generateLocationErrorItem(item));
        }
        break;
      default:
        break;
    }
  };

  handleItemNotFound = (item) => {
    const { onToggleDialog } = this.props;
    onToggleDialog(DialogTypes.NEW_ITEM_DIALOG, item);
  };

  handleLocationError = (item) => {
    const { onToggleDialog } = this.props;
    onToggleDialog(DialogTypes.LOCATION_ERROR_DIALOG, item);
  };

  render() {
    const {
      user, inventoryItems, storeSession, isDialogOpen, dialogProps, dialogType,
      onToggleDialog, onCheckStatus, isSubmitting, isLoading,
    } = this.props;

    const containsErrors = IndexUtils.hasErrorsInItems(inventoryItems);
    const submitBtnProps = {
      disabled: containsErrors,
      primary: !containsErrors,
    };

    if ((storeSession.submitSessionStatus === SUBMITTING) && isSubmitting) {
      onCheckStatus(storeSession.id);
    }

    if (isSubmitting && isLoading) {
      return <Loader text="Submitting session..." />;
    }

    const data = IndexUtils.generateData(inventoryItems);

    return (
      <div>
        <Head title="Home" />
        <Card main>
          <CardHeader>{storeSession.name}</CardHeader>
          <CardBody>
            <SessionToolbar user={user} />
          </CardBody>
        </Card>
        <ModalStyles
          isOpen={isDialogOpen}
          ariaHideApp={false}
        >
          <Container>
            <DialogContainer type={dialogType} props={dialogProps} />
          </Container>
        </ModalStyles>
        <TableList
          columns={IndexUtils.columns}
          data={data}
          filterBy="inventoryItemType"
          handleItemClick={this.handleItemClick}
        />

        {
          containsErrors
            ? <ErrorMessage><FormattedMessage id="erp_error" /></ErrorMessage>
            : null
        }
        <ActionButtonGroup>
          <Button secondary type="button" onClick={() => onToggleDialog(DialogTypes.RESTART_DIALOG)}>Restart/Discard</Button>
          <Button primary type="button" onClick={() => onToggleDialog(DialogTypes.SAVE_DIALOG)}>Save</Button>
          <Button {...submitBtnProps} type="button" onClick={() => onToggleDialog(DialogTypes.SUBMIT_DIALOG)}>Submit</Button>
        </ActionButtonGroup>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  dialogProps: state.dialog.dialogProps,
  dialogType: state.dialog.dialogType,
  inventoryItems: state.inventoryItems,
  isDialogOpen: state.dialog.isDialogOpen,
  isLoading: state.erpSubmit.isLoading,
  isSubmitting: state.erpSubmit.isSubmitting,
  storeSession: state.storeSession,
  user: state.user.entity,
});

const mapActionsToProps = {
  onCheckStatus: submitToErpStatus,
  onToggleDialog: toggleDialog,
};

export default connect(mapStateToProps, mapActionsToProps)(Home);
