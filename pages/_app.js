import React from 'react';
import App, { Container } from 'next/app';

import withRedux from 'next-redux-wrapper';
import withReduxSaga from 'next-redux-saga';
import { Provider } from 'react-redux';

import Page from '../components/Page';
import configureStore from '../store/store';
import { fetchUser } from '../store/actions/user';
import IntlProviderWrapper from '../components/IntlProviderWrapper';
import 'core-js';

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    if (ctx.req) {
      const { userId } = ctx.req.session.store ? ctx.req.session.store : ctx.req.query;
      ctx.store.dispatch(fetchUser(userId, ctx.req.headers.cookie));
    }

    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};

    return { pageProps };
  }

  render() {
    const { Component, pageProps, store } = this.props;

    return (
      <Container>
        <Provider store={store}>
          <IntlProviderWrapper>
            <Page>
              <Component {...pageProps} />
            </Page>
          </IntlProviderWrapper>
        </Provider>
      </Container>
    );
  }
}

export default withRedux(configureStore)(withReduxSaga(MyApp));
