import React from 'react';
import { connect } from 'react-redux';

import Head from '../components/Head';
import { Card, CardHeader, CardBody } from '../components/styles/Card';
import ConnectedLoginForm from '../components/LoginForm';
import LoginStyles from '../components/styles/Login/LoginStyles';

const Login = ({ user, cookie }) => (
  <LoginStyles>
    <Head title="Login" />
    <Card main>
      <CardHeader />
      <CardBody>
        <ConnectedLoginForm user={user} cookie={cookie} />
      </CardBody>
    </Card>
  </LoginStyles>
);

Login.getInitialProps = ({ req }) => {
  const { cookie } = req.headers;
  return { cookie };
};

const mapStateToProps = state => ({ user: state.user.entity });

export default connect(mapStateToProps)(Login);
