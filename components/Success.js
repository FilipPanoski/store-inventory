import React from 'react';
import { Card, CardBody, CardHeader } from './styles/Card';
import DialogContent from './styles/Modal/DialogContent';
import Like from './styles/Icons/Like';
import ActionButtonGroup from './styles/ActionButtonGroup/ActionButtonGroup';
import Button from './styles/Button/Button';

const SuccessMessage = () => (
  <Card>
    <CardHeader>THANK YOU!</CardHeader>
    <CardBody>
      <DialogContent>
        <Like />
        <div className="dialog-text dialog-text--success">Thank you!</div>
        <div className="dialog-text dialog-text--sucess">You have successfully submitted the scanned items.</div>
      </DialogContent>
    </CardBody>
    <ActionButtonGroup>
      <Button type="button">Close</Button>
    </ActionButtonGroup>
  </Card>
);

export default SuccessMessage;
