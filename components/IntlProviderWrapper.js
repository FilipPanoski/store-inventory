import React from 'react';
import { connect } from 'react-redux';
import { IntlProvider, addLocaleData } from 'react-intl';

import enLocale from 'react-intl/locale-data/en';
import seLocale from 'react-intl/locale-data/se';
import dkLocale from 'react-intl/locale-data/da';

import enLang from '../translations/en.json';
import seLang from '../translations/se.json';
import dkLang from '../translations/dk.json';

const languages = {
  da: dkLang,
  en: enLang,
  se: seLang,
};

addLocaleData([...enLocale, ...seLocale, ...dkLocale]);

class IntlProviderWrapper extends React.PureComponent {
  render() {
    const { user, children } = this.props;
    const userLang = user.language ? user.language.toLowerCase() : process.env.LOCALE;

    return (
      <IntlProvider locale={userLang} messages={languages[userLang]}>
        {children}
      </IntlProvider>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.entity,
});

export default connect(mapStateToProps)(IntlProviderWrapper);
