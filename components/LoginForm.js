import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { FormattedMessage } from 'react-intl';
import { Form, Button } from './styles/Form';
import { ErrorMessage } from './styles/ErrorMessage';
import { Title } from './styles/Typography';
import { startSession } from '../store/actions/storeSession';
import { ERROR_GETTING_RESULTS } from '../utils/errors';

import StoreSuggestion from './StoreSuggestion';
import Loader from './Loader';

export class LoginForm extends Component {
  static propTypes = {
    user: PropTypes.shape({
      costCenter: PropTypes.string,
      country: PropTypes.string,
      language: PropTypes.string,
      name: PropTypes.string,
      userId: PropTypes.string,
    }),
  };

  static defaultProps = {
    user: null,
  };

  state = {
    store: {},
  };

  saveToState = (value) => {
    this.setState({ store: value });
  };

  startSession = (e) => {
    e.preventDefault();
    const { user, cookie, onStartSession } = this.props;
    const { store } = this.state;
    const isLoading = true;

    onStartSession(store, user, cookie, isLoading);
  };

  render() {
    const { user, storeSession } = this.props;

    if (storeSession.isLoading) {
      return <Loader />;
    }

    return (
      <div>
        <Title textCenter>
          <FormattedMessage
            id="login.welcome_message"
            value={user.name}
          />
          {user && user.name}
        </Title>
        <Form onSubmit={this.startSession} autoComplete="off" login>
          <StoreSuggestion
            id="storeNumber"
            suggestedStores={user.costCenters}
            onChange={this.saveToState}
          />
          <ErrorMessage>
            {storeSession.error && storeSession.error.status === ERROR_GETTING_RESULTS.status
              ? <FormattedMessage id="login.store_not_found_err" />
              : ''}
          </ErrorMessage>
          <div className="text-center">
            <Button login type="submit">Start</Button>
          </div>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  storeSession: state.storeSession,
});

const mapActionsToProps = {
  onStartSession: startSession,
};

export default connect(mapStateToProps, mapActionsToProps)(LoginForm);
