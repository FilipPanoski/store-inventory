import React from 'react';
import Form from 'react-validation/build/form';
import ValidatedButton from 'react-validation/build/button';
import Grid from './styles/Grid/Grid';
import { Col, Row } from './styles/Grid';
import LabeledInput from './LabeledInput';
import ValidatedFormStyle from './styles/Form/ValidatedForm';
import Button from './styles/Button/Button';
import ValidatedInput from './ValidatedInput';
import { isEan, isImei, isQuantity } from '../utils/validations';
import ValidatedButtonStyle from './styles/Button/ValidatedButtonStyle';
import Label from './Label';
import LocationPicker from './LocationPicker';

const NewItemForm = ({
  name,
  productID,
  imei,
  item,
  location,
  quantity,
  ean,
  isSerialized,
  locations,
  editNewItem,
  saveToState,
  saveNewItem,
  toggleNewItemDialog,
}) => {
  const imeiValidations = isItemSerialized => (isItemSerialized ? [] : [isImei]);

  const eanValidations = isItemSerialized => (isItemSerialized ? [isEan] : []);

  const onSave = (e) => {
    e.preventDefault();
    if (item) {
      editNewItem();
    } else {
      saveNewItem();
    }
  };

  const onCancel = (e) => {
    e.preventDefault();
    toggleNewItemDialog();
  };

  return (
    <ValidatedFormStyle>
      <Form onSubmit={e => e.preventDefault()}>
        <Grid>
          <Row>
            <Col default>
              <LabeledInput
                disabled
                type="text"
                id="name"
                name="name"
                placeholder="ITEM NAME"
                value={name}
                onChange={saveToState}
              />
            </Col>
            <Col default>
              <LabeledInput
                disabled
                type="text"
                id="productId"
                name="productID"
                placeholder="PRODUCT ID"
                value={productID}
                onChange={saveToState}
              />
            </Col>
            <Col default>
              <ValidatedInput
                disabled={isSerialized}
                label="IMEI"
                type="text"
                id="imei"
                name="imei"
                placeholder="IMEI"
                value={imei}
                onChange={saveToState}
                validations={imeiValidations(isSerialized)}
              />
            </Col>
          </Row>
          <Row>
            <Col default>
              <Label
                id="location"
                name="location"
                component={(
                  <LocationPicker
                    name="location"
                    disabled={item}
                    saveLocation={saveToState}
                    location={location}
                    locations={locations}
                    handleDropdownKeyDown={() => {}}
                  />
                )}
              />
            </Col>
            <Col default>
              <ValidatedInput
                disabled={isSerialized || item}
                label="QUANTITY"
                type="number"
                id="quantity"
                name="quantity"
                placeholder="ex: 1"
                value={quantity}
                onChange={saveToState}
                validations={[isQuantity]}
              />
            </Col>
            <Col default>
              <ValidatedInput
                label="EAN"
                disabled={!isSerialized}
                type="text"
                id="ean"
                name="ean"
                placeholder="EAN"
                value={ean}
                onChange={saveToState}
                validations={eanValidations(isSerialized)}
              />
            </Col>
          </Row>
        </Grid>
        <div className="buttons-wrapper">
          <ValidatedButtonStyle>
            <ValidatedButton onClick={onSave}>Save</ValidatedButton>
          </ValidatedButtonStyle>
          <Button onClick={onCancel}>Cancel</Button>
        </div>
      </Form>
    </ValidatedFormStyle>
  );
};

export default NewItemForm;
