import React from 'react';
import { connect } from 'react-redux';
import { existingItemSave, newItemSave } from '../store/actions/inventoryItems';
import generateNewItem from '../utils/jsonGenerator';
import formatDateAndTime from '../utils/formatDateTime';
import { SERIALIZED } from '../utils/inventoryItemTypes';
import { ERROR_ON_LOCATION, SCAN_OK, ITEM_NOT_FOUND } from '../utils/inventoryItemStatuses';
import specialTextInputs from '../utils/specialTextInputs';
import isAlphaNumeric from '../utils/isAlphaNumeric';


class ItemIdInputWrapper extends React.PureComponent {
  componentDidMount() {
    this.bindScannerEvent();
  }

  bindScannerEvent = () => {
    document.addEventListener('keydown', (e) => {
      this.keyDownAttached = true;
      const { itemID, setScannerId, resetStateAfterScan } = this.props;
      const targetName = e.target.name;
      const textInput = e.key || String.fromCharCode(e.keyCode);
      const isScanner = typeof targetName === 'undefined';
      if (
        isScanner
        && textInput
        && specialTextInputs.indexOf(textInput) === -1
        && isAlphaNumeric(textInput)
        && !e.altKey && !e.ctrlKey && !e.metaKey
      ) {
        setScannerId(itemID, textInput);
      }
      if (textInput === 'Enter' && itemID) {
        this.checkItem();
        resetStateAfterScan();
      }
    });
  };

  handleItemFound = (foundItem) => {
    const {
      locations, onExistingItemSave,
      scanAction, quantity,
    } = this.props;
    const scanDateTime = formatDateAndTime(new Date());
    if (
      scanAction === 'Unregister'
      && foundItem.totalQuantity - quantity < 0
    ) {
      return;
    }

    if (
      scanAction === 'Register'
      && foundItem.inventoryItemType === SERIALIZED
      && foundItem.totalQuantity > 0
    ) {
      return;
    }

    let newItemQuantity = quantity;
    if (foundItem.inventoryItemType === SERIALIZED && newItemQuantity > 1) {
      newItemQuantity = 1;
    }

    const { location } = this.props;

    const wrongLocation = foundItem.inventoryLocation.locationNumber !== location.locationNumber;
    const status = wrongLocation ? ERROR_ON_LOCATION : SCAN_OK;
    const filteredLocation = locations.filter(l => l.locationNumber === location.locationNumber)[0];
    onExistingItemSave({
      inventoryItemId: foundItem.id,
      inventoryLocation: filteredLocation,
      quantity:
        scanAction === 'Register' ? newItemQuantity : -1 * newItemQuantity,
      scanDateTime,
      status,
    });
  };

  handleItemNotFound = () => {
    const {
      locations, onNewItemSave, itemID, scanAction,
      storeSession, quantity, location,
    } = this.props;
    const scanDateTime = formatDateAndTime(new Date());

    if (scanAction === 'Unregister') {
      return;
    }

    const itemLocation = locations.filter(
      l => l.locationNumber === location.locationNumber,
    )[0];

    onNewItemSave(
      generateNewItem({
        imei: itemID,
        isSerialized: null,
        location: itemLocation,
        quantity: scanAction === 'Register' ? quantity : -1 * quantity,
        scanDateTime,
        session: storeSession,
        status: ITEM_NOT_FOUND,
        totalQuantity: scanAction === 'Register' ? quantity : -1 * quantity,
      }),
    );
  };

  checkItem = () => {
    const {
      inventoryItems,
      itemID,
    } = this.props;
    const foundItem = inventoryItems.list.filter(
      item => item.productId === itemID || item.imei === itemID || item.ean === itemID,
    )[0];

    if (foundItem) {
      this.handleItemFound(foundItem);
    } else {
      this.handleItemNotFound();
    }
  };

  render() {
    const { itemID, saveToState } = this.props;
    return (
      <input
        ref={(input) => {
          this.itemIdInputRef = input;
        }}
        type="text"
        className="form-control"
        id="itemID"
        name="itemID"
        placeholder="ITEM ID"
        value={itemID}
        onChange={saveToState}
      />
    );
  }
}

const mapStateToProps = state => ({
  inventoryItems: state.inventoryItems,
  locations: state.inventoryLocations.list,
  storeSession: state.storeSession,
});

const mapActionsToProps = {
  onExistingItemSave: existingItemSave,
  onNewItemSave: newItemSave,
};

export default connect(mapStateToProps, mapActionsToProps)(ItemIdInputWrapper);
