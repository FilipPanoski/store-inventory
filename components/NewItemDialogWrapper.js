import React from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import DialogTypes from '../utils/DialogTypes';
import { toggleDialog } from '../store/actions/dialog';
import Plus from './styles/Icons/Plus';


class NewItemDialogWrapper extends React.PureComponent {
  render() {
    const { onToggleDialog } = this.props;

    return (
      <div className="wrapper">
        <span
          tabIndex={0}
          role="button"
          className="new-item"
          onKeyPress={() => onToggleDialog(DialogTypes.NEW_ITEM_DIALOG)
          }
          onClick={() => onToggleDialog(DialogTypes.NEW_ITEM_DIALOG)}
        >
          <FormattedMessage id="toolbar.label.new_item" />
          <Plus />
        </span>
      </div>
    );
  }
}


const mapActionsToProps = {
  onToggleDialog: toggleDialog,
};

export default connect(null, mapActionsToProps)(NewItemDialogWrapper);
