import React from 'react';
import { FormattedMessage } from 'react-intl';
import LabeledInput from './LabeledInput';
import ItemSuggestion from './ItemSuggestion';
import SelectedItem from './SelectedItem';
import SearchStyles from './styles/Inputs/SearchStyles';

class Search extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      productId: '',
      productName: '',
      search: props.item ? (props.item.toUpperCase()) : (''),
    };
  }

  filterItems = (filterBy) => {
    const { items } = this.props;
    let suggestions = [];

    const itemID = filterBy.toUpperCase();
    if (itemID.length > 2) {
      suggestions = items.filter(
        item => (item.productId ? item.productId.toUpperCase().indexOf(itemID) !== -1 : false)
          || (item.ean ? (item.ean.toUpperCase().indexOf(itemID) !== -1) : false)
          || (item.imei ? item.imei.toUpperCase().indexOf(itemID) !== -1 : false),
      );
    }
    return suggestions;
  };

  findItem = (itemValue) => {
    const { hasError } = this.state;
    const filteredItems = this.filterItems(itemValue);

    if (filteredItems.length) {
      this.onSelectItem(filteredItems[0]);
    } else if (!hasError) {
      this.setState({ hasError: true });
    }
  };

  onSelectItem = (selectedItem) => {
    this.setState({
      productId: selectedItem.productId,
      productName: selectedItem.productName,
    });

    const { onSelectItem } = this.props;
    onSelectItem(selectedItem);
  };

  validateSearchError = (suggestions) => {
    const { search } = this.state;

    if (search.length > 2) {
      return !suggestions.length;
    }
    return false;
  };

  render() {
    const { search, productName, productId } = this.state;
    const {
      isItemSelected, isSerialized, deselectItem, item,
    } = this.props;
    const suggestions = this.filterItems(search);
    const hasError = this.validateSearchError(suggestions);
    const isPrimary = suggestions.length === 0;

    if (item && !isItemSelected) {
      this.findItem(item);
    }

    return (
      <SearchStyles primary={isPrimary}>
        {
          isItemSelected
            ? (
              <SelectedItem
                name="selected-item"
                id="selectedItem"
                foundItem={item}
                productName={productName}
                productId={productId}
                isSerialized={isSerialized}
                deselectItem={deselectItem}
              />
            )
            : (
              <div>
                <LabeledInput
                  type="text"
                  id="search"
                  name="search"
                  placeholder=""
                  value={search}
                  onChange={e => this.setState({ search: (e.target.value).toUpperCase() })}
                />
                <ItemSuggestion suggestions={suggestions} onSelectItem={this.onSelectItem} />
                {hasError
                  ? <div className="search-error"><FormattedMessage id="search.product_error" /></div> : null}
              </div>
            )
        }
      </SearchStyles>
    );
  }
}

export default Search;
