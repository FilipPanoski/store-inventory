import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Card, CardHeader, CardBody } from './styles/Card';
import DialogContent from './styles/Modal/DialogContent';
import { Form } from './styles/Form';
import { Grid, Row, Col } from './styles/Grid';
import LabeledInput from './LabeledInput';
import LocationPicker from './LocationPicker';
import ActionButtonGroup from './styles/ActionButtonGroup/ActionButtonGroup';
import Button from './styles/Button/Button';


const LocationDialogContent = ({
  erpLocation, location, locations, onToggleDialog, onConfirmLocation, onSetLocation,
}) => (
  <Card formDesktop>
    <CardHeader><FormattedMessage id="confirm.location.dialog_header" /></CardHeader>
    <CardBody>
      <DialogContent>
        <Form>
          <Grid>
            <Row>
              <Col>
                <LabeledInput
                  disabled
                  label="ERP LOCATION"
                  type="text"
                  id="label.old_location"
                  name="oldLocation"
                  placeholder="ERP LOCATION"
                  value={erpLocation}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <LocationPicker
                  validated
                  withLabel
                  location={location}
                  locations={locations}
                  saveLocation={onSetLocation}
                  name="label.current_location"
                />
              </Col>
            </Row>
          </Grid>
        </Form>
      </DialogContent>
    </CardBody>

    <ActionButtonGroup>
      <Button primary type="button" onClick={() => onConfirmLocation()}>Submit</Button>
      <Button type="button" onClick={() => onToggleDialog()}>Cancel</Button>
    </ActionButtonGroup>
  </Card>
);

export default LocationDialogContent;
