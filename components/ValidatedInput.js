import React from 'react';
import Input from 'react-validation/build/input';
import Label from './Label';

const ValidatedInput = ({
  disabled, id, name, onChange, placeholder, type, validations, value,
}) => (
  <span>
    <Label
      id={id}
      name={name}
      component={(
        <Input
          isChanged
          isUsed
          disabled={disabled}
          type={type}
          className="form-control"
          id={id}
          name={name}
          placeholder={placeholder}
          value={value}
          onChange={onChange}
          validations={validations}
        />
)}
    />
  </span>
);

export default ValidatedInput;
