import React from 'react';
import { connect } from 'react-redux';
import { Card, CardBody, CardHeader } from './styles/Card';
import { Button } from './styles/Button';
import Search from './Search';
import NewItemForm from './NewItemForm';
import { getProductCatalogue } from '../store/actions/productCatalogue';
import { newItemEdit, newItemSave } from '../store/actions/inventoryItems';
import generateNewItem, { editNewItem } from '../utils/jsonGenerator';
import { ERROR_FIX, SCAN_OK } from '../utils/inventoryItemStatuses';
import formatDateAndTime from '../utils/formatDateTime';
import { toggleDialog } from '../store/actions/dialog';

export class NewItemDialog extends React.Component {
  constructor(...args) {
    super(...args);
    const { locations, dialogProps } = this.props;

    this.state = {
      ean: '',
      imei: '',
      isItemSelected: false,
      isSerialized: false,
      item: dialogProps,
      location: dialogProps ? dialogProps.location : locations[0],
      name: '',
      productID: '',
      quantity: '',
    };
  }

  componentDidMount() {
    const { onGetProductCatalogue } = this.props;
    onGetProductCatalogue();
  }

  saveToState = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSelectItem = (selectedItem) => {
    const { dialogProps } = this.props;

    this.setState({
      ean: selectedItem.ean,
      imei: selectedItem.imei,
      isItemSelected: true,
      isSerialized: selectedItem.serializedProduct,
      name: selectedItem.productName,
      productID: selectedItem.productId,
      quantity: dialogProps && !selectedItem.serializedProduct ? (dialogProps.quantity) : (1),
    });
  };

  deselectItem = () => {
    this.setState({ isItemSelected: false });
  };

  saveNewItem = () => {
    const {
      isSerialized, imei, ean, name,
      productID, quantity, location,
    } = this.state;
    const { onToggleDialog, onSaveNewItem, storeSession } = this.props;

    const newItem = generateNewItem({
      ean: ean || null,
      imei: imei || null,
      isSerialized: isSerialized ? 1 : 0,
      location,
      productId: productID,
      productName: name,
      quantity,
      scanDateTime: formatDateAndTime(new Date()),
      session: storeSession,
      status: SCAN_OK,
      totalQuantity: null,
    });

    onSaveNewItem(newItem);
    onToggleDialog();
  };

  editNewItem = () => {
    const {
      isSerialized, item, imei, ean,
      name, productID, quantity, location,
    } = this.state;
    const { onToggleDialog, onEditNewItem, storeSession } = this.props;

    const newItem = editNewItem({
      ean: ean || null,
      id: item.id,
      imei: imei || null,
      isSerialized: isSerialized ? 1 : 0,
      location,
      productId: productID,
      productName: name,
      quantity,
      scanDateTime: formatDateAndTime(new Date()),
      scanItemEvent: ERROR_FIX,
      session: storeSession,
      status: SCAN_OK,
      totalQuantity: null,
    });

    onEditNewItem(newItem);
    onToggleDialog();
  };

  render() {
    const { locations, productCatalogue, onToggleDialog } = this.props;
    const { isItemSelected, isSerialized, item } = this.state;

    return (
      <Card formDesktop>
        <CardHeader>NEW ITEM</CardHeader>
        <CardBody modal>
          <Search
            items={productCatalogue}
            item={item && item.itemId}
            onSelectItem={this.onSelectItem}
            deselectItem={this.deselectItem}
            isItemSelected={isItemSelected}
            isSerialized={isSerialized}
          />
          {
            isItemSelected
              ? (
                <NewItemForm
                  {...this.state}
                  editNewItem={this.editNewItem}
                  saveToState={this.saveToState}
                  saveNewItem={this.saveNewItem}
                  locations={locations}
                  toggleNewItemDialog={onToggleDialog}
                />
              ) : null
          }
          <div className="button-wrapper">
            {isItemSelected ? null : <Button onClick={() => onToggleDialog()}>CANCEL</Button>}
          </div>
        </CardBody>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  locations: state.inventoryLocations.list,
  productCatalogue: state.productCatalogue.productCatalogue,
  storeSession: state.storeSession,
});

const mapActionsToProps = {
  onEditNewItem: newItemEdit,
  onGetProductCatalogue: getProductCatalogue,
  onSaveNewItem: newItemSave,
  onToggleDialog: toggleDialog,
};

export default connect(mapStateToProps, mapActionsToProps)(NewItemDialog);
