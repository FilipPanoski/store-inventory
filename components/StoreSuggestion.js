import React from 'react';
import * as PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import StoreSuggestionStyles from './styles/Outputs/StoreSuggestionStyles';

class StoreSuggestion extends React.Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    suggestedStores: PropTypes.shape({}),
  };

  static defaultProps = {
    suggestedStores: null,
  };

  state = {
    openSuggestion: false,
    storeNumber: '',
  };

  onChange = (e) => {
    const storeNumber = e.target.value;
    this.saveStore({ storeName: null, storeNumber });
  };

  useSuggestion = (suggestedStore) => {
    this.saveStore(suggestedStore);
  };

  saveStore = (store) => {
    const { onChange } = this.props;
    this.setState({ openSuggestion: false, storeNumber: store.storeNumber });
    onChange(store);
  };

  openSuggestion = () => {
    this.setState({ openSuggestion: true });
  };

  render() {
    const { id, suggestedStores } = this.props;
    const { openSuggestion, storeNumber } = this.state;

    let suggestedStore = null;
    if (suggestedStores) {
      const suggestedStoreNumbers = Object.keys(suggestedStores);
      const suggestedStoreNames = suggestedStoreNumbers
        .map(store => suggestedStores[store]);

      suggestedStore = { storeName: suggestedStoreNames[0], storeNumber: suggestedStoreNumbers[0] };
    }

    return (
      <StoreSuggestionStyles>
        <label htmlFor="storeNumber" className="form-control-label">
          <FormattedMessage id="login.store_no" />
          <input
            type="text"
            className="form-control"
            id={id}
            value={storeNumber}
            onChange={this.onChange}
            onFocus={this.openSuggestion}
            required
          />
        </label>

        {openSuggestion && suggestedStore && (
          <div className="suggestion">
            <div className="suggestion__text"><FormattedMessage id="login.store_suggestion" /></div>
            <div
              className="suggestion__item"
              onClick={() => this.useSuggestion(suggestedStore)}
              onKeyPress={() => this.useSuggestion(suggestedStore)}
              role="button"
              tabIndex={-1}
            >
              {suggestedStore.storeNumber}
            </div>
          </div>
        )}
      </StoreSuggestionStyles>
    );
  }
}

export default StoreSuggestion;
