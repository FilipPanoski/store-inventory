import React from 'react';
import { Card, CardBody, CardHeader } from './styles/Card';
import DialogContent from './styles/Modal/DialogContent';
import Dislike from './styles/Icons/Dislike';
import ActionButtonGroup from './styles/ActionButtonGroup/ActionButtonGroup';
import Button from './styles/Button/Button';

const ErrorMessage = () => (
  <Card>
    <CardHeader>ERROR!</CardHeader>
    <CardBody>
      <DialogContent>
        <Dislike />
        <div className="dialog-text dialog-text--success">Uh oh!</div>
        <div className="dialog-text dialog-text--success">There was an error submitting the scanned items!</div>
        <div className="dialog-text dialog-text--sucess">Please try again!</div>
      </DialogContent>
    </CardBody>
    <ActionButtonGroup>
      <Button type="button" special>Go back</Button>
    </ActionButtonGroup>
  </Card>
);

export default ErrorMessage;
