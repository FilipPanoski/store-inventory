import React from 'react';
import { connect } from 'react-redux';
import { toggleDialog } from '../store/actions/dialog';
import { existingItemPut } from '../store/actions/inventoryItems';
import formatDateAndTime from '../utils/formatDateTime';
import { SCAN_OK, ERROR_FIX } from '../utils/inventoryItemStatuses';
import LocationDialogContent from './LocationDialogContent';
import { putNewItem } from '../utils/jsonGenerator';

export class ConfirmLocationDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      location: props.dialogProps.errorLocation,
    };
  }

  setLocation = (e) => {
    this.setState({ location: e.target.value });
  };

  confirmLocation = () => {
    const {
      dialogProps, onItemPut, onToggleDialog, storeSession,
    } = this.props;
    const { location } = this.state;

    const putItem = putNewItem({
      id: dialogProps.id,
      imei: dialogProps.imei,
      itemQuantity: dialogProps.quantity,
      location,
      productId: dialogProps.productId,
      productName: dialogProps.productName,
      quantity: dialogProps.quantity,
      scanDateTime: formatDateAndTime(new Date()),
      scanItemEvent: ERROR_FIX,
      sessionId: storeSession.id,
      status: SCAN_OK,
      totalQuantity: dialogProps.quantity,
    });

    onItemPut(putItem);
    onToggleDialog();
  };

  render() {
    const { onToggleDialog, dialogProps, locations } = this.props;
    const { location } = this.state;

    return (
      <LocationDialogContent
        erpLocation={dialogProps.location.locationName}
        location={location}
        locations={locations}
        onToggleDialog={onToggleDialog}
        onConfirmLocation={this.confirmLocation}
        onSetLocation={this.setLocation}
      />
    );
  }
}

const mapStateToProps = state => ({
  locations: state.inventoryLocations.list,
  storeSession: state.storeSession,
});

const mapActionsToProps = {
  onItemPut: existingItemPut,
  onToggleDialog: toggleDialog,
};

export default connect(mapStateToProps, mapActionsToProps)(ConfirmLocationDialog);
