import React from 'react';
import { FormattedMessage } from 'react-intl';
import ToggleButton from './ToggleButton';


const NotificationsToggle = ({ toggleChange }) => (
  <div className="wrapper">
    <span className="toolbar-item">
      <FormattedMessage id="toolbar.label.notifications" />
    </span>
    <ToggleButton
      leftValue="On"
      rightValue="Off"
      field="notifications"
      toggleChange={toggleChange}
    />
  </div>
);

export default NotificationsToggle;
