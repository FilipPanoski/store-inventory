import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Form } from './styles/Form';
import { Grid, Row, Col } from './styles/Grid';
import LocationPicker from './LocationPicker';
import QuantityPicker from './QuantityPicker';
import NewItemDialogWrapper from './NewItemDialogWrapper';
import ItemIdInputWrapper from './ItemIdInputWrapper';
import isAlphaNumeric from '../utils/isAlphaNumeric';
import RegisterToggle from './RegisterToggle';
import NotificationsToggle from './NotificationsToggle';

class SessionToolbar extends Component {
  itemIdInputRef = null;

  constructor(props) {
    super(props);
    this.state = {
      itemID: '',
      location: props.locations[0],
      quantity: 1,
      scanAction: 'Register',
    };
  }

  componentWillUnmount() {
    if (this.keyDownAttached) {
      document.removeEventListener('keydown', () => undefined);
    }
  }

  saveToState = (e) => {
    if (isAlphaNumeric(e.target.value)) {
      this.setState({ [e.target.name]: e.target.value });
    }
  };

  setScannerId = (itemID, textInput) => {
    this.setState({ itemID: itemID + textInput });
  };

  resetStateAfterScan = () => {
    this.setState({
      itemID: '',
      quantity: 1,
    });
  };

  saveLocation = (e) => {
    this.setState({ location: e.target.value });
  };

  saveQuantity = (quantity) => {
    this.setState({ quantity: parseInt(quantity, 10) });
  };

  handleDropdownKeyDown = (e) => {
    e.target.blur();
    this.focusOnItemId();
  };

  focusOnItemId = () => {
    if (this.itemIdInputRef) {
      this.itemIdInputRef.focus();
    }
  };

  toggleChange = (field, value) => {
    this.setState({ [field]: value });
  };

  render() {
    const { locations } = this.props;
    const {
      location, itemID, quantity, scanAction,
    } = this.state;

    return (
      <Form onSubmit={e => e.preventDefault()}>
        <Grid>
          <Row>
            <Col>
              <LocationPicker
                saveLocation={this.saveLocation}
                location={location}
                locations={locations}
                handleDropdownKeyDown={this.handleDropdownKeyDown}
              />
            </Col>
            <Col special>
              <QuantityPicker
                quantity={quantity}
                saveQuantity={this.saveQuantity}
                handleDropdownKeyDown={this.handleDropdownKeyDown}
              />
            </Col>
            <Col>
              <ItemIdInputWrapper
                itemID={itemID}
                location={location}
                quantity={quantity}
                scanAction={scanAction}
                saveToState={this.saveToState}
                setScannerId={this.setScannerId}
                resetStateAfterScan={this.resetStateAfterScan}
              />
            </Col>
            <Col special>
              <RegisterToggle toggleChange={this.toggleChange} />
            </Col>
            <Row>
              <Col />
              <Col special>
                <NotificationsToggle toggleChange={this.toggleChange} />
              </Col>
              <Col special />
              <Col special>
                <NewItemDialogWrapper />
              </Col>
            </Row>
          </Row>
        </Grid>
      </Form>
    );
  }
}

const mapStateToProps = state => ({
  locations: state.inventoryLocations.list,
});

export default connect(mapStateToProps)(SessionToolbar);
