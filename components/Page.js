import React from 'react';
import styled, { createGlobalStyle, ThemeProvider } from 'styled-components';
import { connect } from 'react-redux';
import Header from './Header';
import Media from './styles/Responsive/MediaQueries';

const theme = {
  black: '#393939',
  darkGray: '#999999',
  green: '#00CA46',
  lightGray: '#EEEEEE',
  orange: '#FD930C',
  red: '#EC2E3A',
};

const GlobalStyles = createGlobalStyle`
  @font-face {
    font-family: Helvetica Neue;
    src: url('/static/fonts/HelveticaNeueLTW01-55Roman.woff2') format('woff2');
  }
  
  @font-face {
    font-family: Helvetica Neue Bold;
    src: url('/static/fonts/HelveticaNeueLTW01-75Bold.woff2') format('woff2');
  }
  
  @font-face {
    font-family: Helvetica Neue Medium;
    src: url('/static/fonts/HelveticaNeueLTW01-65Medium.woff2') format('woff2');
  }
  
  html {
    box-sizing: border-box;
    font-size: 16px;
    font-family: "Helvetica Neue", sans-serif;
  }
  
  *, *:before, *:after {
    box-sizing: inherit;
  }

  body {
    padding: 0;
    margin: 0;
    background-color: #f8f8f8;
  }

  a {
    text-decoration: none;
    color: ${theme.black};
  }
  
  .text-center {
    text-align: center;
  }
`;

const Container = styled.div`
  width: 1130px;
  margin: 130px auto 40px;

  ${Media.desktop`
    width: 100%;    
    margin: 60px auto;
  `}
  ${Media.tablet`
    width: 100%;
    margin: 60px auto;
  `}
  ${Media.phone`
    width: 100%;
    margin: 40px auto;
  `}
`;

const Page = ({ children, user }) => (
  <ThemeProvider theme={theme}>
    <React.Fragment>
      <GlobalStyles />
      <Header user={user} />
      <Container>{children}</Container>
    </React.Fragment>
  </ThemeProvider>
);

const mapStateToProps = state => ({ user: state.user.entity });

export default connect(mapStateToProps)(Page);
