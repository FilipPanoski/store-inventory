import React from 'react';
import styled from 'styled-components';
import * as PropTypes from 'prop-types';
import UserIcon from './styles/Icons/UserIcon';
import Media from './styles/Responsive/MediaQueries';

const HeaderStyles = styled.div`
  position: relative;
  height: 72px;
  padding: 5px;
  color: #000;
  background-color: #fff;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.16);

  text-align: center;

  ${Media.desktop`
    width: 100%; 
    height: 50px;
  `}
  ${Media.tablet`
    width: 100%; 
    height: 50px;
  `}
`;

const HeaderMenu = styled.div`
  position: absolute;
  top: 50%;
  right: 20px;
  transform: translateY(-50%);
`;

const Logo = styled.img`
  width: 60px;
  height: 60px;
  display: block;
  text-align: center;
  margin: 0 auto;

  ${Media.desktop`
    width: 40px; 
    height: 40px;
  `}
`;

const User = styled.div`
  display: block;

  .icon {
    display: block;
    margin: 0 auto;
    text-align: center;
  }

  .user-name {
    font-family: 'Helvetica Neue', sans-serif;
    font-size: 14px;
    font-weight: normal;
    line-height: 22px;

    ${Media.desktop`font-size: 12px;`}
  }

  ${Media.desktop`
    margin-right: 16px;
  `}
`;

const Header = ({ user }) => (
  <HeaderStyles>
    <Logo src="/static/images/logo.png" alt="Tre Logo" />
    <HeaderMenu>
      <User>
        <UserIcon />
        {user && <div className="user-name">{user.name}</div>}
      </User>
    </HeaderMenu>
  </HeaderStyles>
);

Header.propTypes = {
  user: PropTypes.shape({
    costCenter: PropTypes.string,
    country: PropTypes.string,
    language: PropTypes.string,
    name: PropTypes.string,
    userId: PropTypes.string,
  }),
};

Header.defaultProps = {
  user: null,
};

export default Header;
