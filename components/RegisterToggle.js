import React from 'react';
import ToggleButton from './ToggleButton';


const RegisterToggle = ({ toggleChange }) => (
  <ToggleButton
    regular
    leftValue="Register"
    rightValue="Unregister"
    field="scanAction"
    toggleChange={toggleChange}
  />
);

export default RegisterToggle;
