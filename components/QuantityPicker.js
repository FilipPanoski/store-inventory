import React from 'react';
import { FormattedMessage } from 'react-intl';
import range from '../utils/range';


class QuantityPicker extends React.PureComponent {
  setQuantity = (operation) => {
    const { quantity, saveQuantity } = this.props;
    if (operation === 'increase') {
      if (quantity < 200) saveQuantity(quantity + 1);
    } else if (quantity > 1) saveQuantity(quantity - 1);
  };

  render() {
    const {
      quantity, saveQuantity, handleDropdownKeyDown,
    } = this.props;
    const quantityOptions = range(1, 200);

    return (
      <div className="wrapper">
        <span className="control-label no-selection">
          <FormattedMessage id="toolbar.label.quantity" />
        </span>
        <select
          className="filter-options"
          value={quantity}
          onKeyDown={e => handleDropdownKeyDown(e)}
          onChange={e => saveQuantity(e.target.value)}
        >
          {quantityOptions.map(item => (
            <option key={item} value={item}>
              {item}
            </option>
          ))}
        </select>
      </div>
    );
  }
}

export default QuantityPicker;
