import React from 'react';
import { FormattedMessage } from 'react-intl';

const Label = ({ id, component, name }) => (
  <label className="form-control-label" htmlFor={name}>
    <div>
      <FormattedMessage className="label-description" id={`label.product_${id}`} />
    </div>
    {component}
  </label>
);

export default Label;
