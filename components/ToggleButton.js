import React from 'react';
import { FormattedMessage } from 'react-intl';
import ToggleButtonStyles from './styles/Button/ToggleButtonStyles';

class ToggleButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: null };
  }

  onChange = (e) => {
    e.target.blur();
    const {
      toggleChange, field, rightValue, leftValue,
    } = this.props;

    // We cant use destructuring props here because
    // the state value is not read correctly in
    // the callback
    // const { value } = this.state;
    this.setState({ value: e.target.checked }, () => toggleChange(
      field,
      this.state.value ? rightValue : leftValue, // eslint-disable-line
    ));
  };

  render() {
    const { regular, leftValue, rightValue } = this.props;
    const inputRef = null;
    return (
      <ToggleButtonStyles regular={regular}>
        <label htmlFor={inputRef} className="toggle-label">
          <input ref={inputRef} type="checkbox" onChange={this.onChange} />
          <span className="back">
            <span className="toggle" />
            <span className="label on"><FormattedMessage id={`toolbar.toggle.${leftValue.toLowerCase()}`} /></span>
            <span className="label off"><FormattedMessage id={`toolbar.toggle.${rightValue.toLowerCase()}`} /></span>
          </span>
        </label>
      </ToggleButtonStyles>
    );
  }
}

export default ToggleButton;
