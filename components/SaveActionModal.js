import React from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Card, CardBody, CardHeader } from './styles/Card';
import ActionButtonGroup from './styles/ActionButtonGroup/ActionButtonGroup';
import Button from './styles/Button/Button';
import { toggleDialog } from '../store/actions/dialog';
import DialogContent from './styles/Modal/DialogContent';
import Save from './styles/Icons/SaveIcon';

class SaveActionModal extends React.PureComponent {
  render() {
    const { onToggleDialog } = this.props;

    return (
      <Card formDesktop>
        <CardHeader><FormattedMessage id="save.modal.header" /></CardHeader>
        <CardBody>
          <DialogContent save>
            <Save />
            <div className="dialog-warning-question"><FormattedMessage id="save.modal.dialog_question" /></div>
            <div className="dialog-text"><FormattedMessage id="save.modal.dialog_text_one" /></div>
            <div className="dialog-text"><FormattedMessage id="save.modal.dialog_text_two" /></div>
          </DialogContent>
        </CardBody>
        <ActionButtonGroup>
          <Button primary type="button">Save</Button>
          <Button type="button" onClick={() => onToggleDialog()}>Cancel</Button>
        </ActionButtonGroup>
      </Card>
    );
  }
}

const mapStateToProps = () => ({});

const mapActionsToProps = {
  onToggleDialog: toggleDialog,
};

export default connect(mapStateToProps, mapActionsToProps)(SaveActionModal);
