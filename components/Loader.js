import React from 'react';
import styled from 'styled-components';

const LoaderStyles = styled.div`
  width: 125px;
  height: 125px;
  text-align: center;
  margin: 0 auto;

  .ball-ring {
    position: absolute;
    width: 125px;
    height: 125px;
    border: 2px solid #8fffbc;
    border-radius: 50%;
    -o-border-radius: 50%;
    -ms-border-radius: 50%;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
  }

  .ball-holder {
    position: absolute;
    width: 110px;
    height: 100px;
    border-radius: 50%;
    left: 6px;
    top: 10px;
    animation: spin 4s linear 0s infinite;
    -o-animation: spin 4s linear 0s infinite;
    -ms-animation: spin 4s linear 0s infinite;
    -webkit-animation: spin 4s linear 0s infinite;
    -moz-animation: spin 4s linear 0s infinite;
  }

  .ball {
    position: absolute;
    left: 0;
    width: 23px;
    height: 23px;
    background-color: #ffffff;
    border: 3px solid #8fffbc;
    border-radius: 19px;
    -o-border-radius: 19px;
    -ms-border-radius: 19px;
    -webkit-border-radius: 19px;
    -moz-border-radius: 19px;
  }
  
  .loading-text {
    line-height: 125px;
    font-size: 12px;
    color: #999999;
  }

  .spinner {
    position: relative;
    width: 125px;
    height: 125px;

    &:after {
      content: ' ';
      display: block;
      border-width: 4px;
      border-style: solid;
      border-radius: 50%;
    }
  }

  .inside-spinner {
    &:after {
      width: 87px;
      height: 87px;
      border-color: #e4e4e4;
      top: 20px;
      left: 18px;
      animation: scale 2s linear 0s infinite;
      -o-animation: scale 2s linear 0s infinite;
      -ms-animation: scale 2s linear 0s infinite;
      -webkit-animation: scale 2s linear 0s infinite;
      -moz-animation: scale 2s linear 0s infinite;
      position: absolute;
    }

    @keyframes scale {
      0% {
        -webkit-transform: scale(0);
        transform: scale(0);
        opacity: 0;
      }

      50% {
        -webkit-transform: scale(0.7);
        transform: scale(0.7);
        opacity: 1;
      }

      100% {
        -webkit-transform: scale(1);
        transform: scale(1);
        opacity: 0;
      }
    }

    @-webkit-keyframes scale {
      0% {
        -webkit-transform: scale(0);
        transform: scale(0);
        opacity: 0;
      }

      50% {
        -webkit-transform: scale(0.7);
        transform: scale(0.7);
        opacity: 1;
      }

      100% {
        -webkit-transform: scale(1);
        transform: scale(1);
        opacity: 0;
      }
    }
  }

  @keyframes spin {
    0% {
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
    }

    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }

  @-webkit-keyframes spin {
    0% {
      -webkit-transform: rotate(0deg);
    }

    100% {
      -webkit-transform: rotate(360deg);
    }
  }
`;

const Loader = ({ text }) => (
  <LoaderStyles>
    <div className="ball-ring">
      <div className="ball-holder">
        <div className="ball" />
      </div>
      <span className="loading-text">
        {text || 'Loading...'}
      </span>
    </div>
    <div className="spinner inside-spinner" />
  </LoaderStyles>
);

export default Loader;
