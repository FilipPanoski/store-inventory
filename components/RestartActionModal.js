import React from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Card, CardBody, CardHeader } from './styles/Card';
import ActionButtonGroup from './styles/ActionButtonGroup/ActionButtonGroup';
import Button from './styles/Button/Button';
import { toggleDialog } from '../store/actions/dialog';
import DialogContent from './styles/Modal/DialogContent';
import File from './styles/Icons/FileIcon';

class RestartActionModal extends React.PureComponent {
  render() {
    const { onToggleDialog } = this.props;

    return (
      <Card formDesktop>
        <CardHeader><FormattedMessage id="restart.modal.header" /></CardHeader>
        <CardBody>
          <DialogContent>
            <File />
            <div className="dialog-warning-question"><FormattedMessage id="restart.modal.dialog_question" /></div>
            <div className="dialog-text"><FormattedMessage id="restart.modal.dialog_text" /></div>
          </DialogContent>
        </CardBody>
        <ActionButtonGroup>
          <Button primary type="button">Start New</Button>
          <Button type="button" onClick={() => onToggleDialog()}>Cancel</Button>
        </ActionButtonGroup>
      </Card>
    );
  }
}

const mapStateToProps = () => ({});

const mapActionsToProps = {
  onToggleDialog: toggleDialog,
};
export default connect(mapStateToProps, mapActionsToProps)(RestartActionModal);
