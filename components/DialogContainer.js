import React from 'react';

const DialogContainer = ({ props, type }) => {
  const DialogComponent = React.lazy(() => import(`${type}`));

  return (
    <React.Suspense fallback={null}>
      <div>
        <DialogComponent dialogProps={props} />
      </div>
    </React.Suspense>
  );
};

export default DialogContainer;
