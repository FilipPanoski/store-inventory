import styled from 'styled-components';

const StoreSuggestionStyles = styled.div`
  position: relative;

  .suggestion {
    position: absolute;
    bottom: -67px;
    left: 0;
    width: 200px;
    padding: 5px 0;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.16);

    background: #fff;

    &__item {
      width: 100%;
      padding: 5px 15px;
      cursor: pointer;

      &:hover {
        background: #f2f2f2;
      }
    }

    &__text {
      padding: 5px 10px;
      font-style: italic;
      font-size: 14px;
      color: #bdbdbd;
    }
  }
`;

export default StoreSuggestionStyles;
