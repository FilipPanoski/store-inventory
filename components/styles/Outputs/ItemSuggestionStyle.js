import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const ItemSuggestionStyle = styled.div`
  padding: 5px 10px;
  background-color: white;
  
  border-left: 1px solid #cccccc;
  border-right: 1px solid #cccccc;
  border-bottom: 1px solid #fd930c;
  
  cursor: pointer;
  
  font-size: 14px;
  font-family: 'Helvetica Neue', sans-serif;
  font-weight: 500;
  line-height: 40px;
  

  &:last-child {
    border-bottom: 1px solid #cccccc;
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  
  .suggestion-name {
    color: #fd930c;
    font-size: 16px;
    font-weight: 700;
    text-transform: uppercase;
    ${Media.phone`
      font-size: 10px;
    `}
  }
  
  .suggestion-id {
    float: right;
    color: black;
    font-weight: 700;
    ${Media.phone`
      font-size: 10px;
    `}
  }
  
  &:hover {
    background-color: #EEEEEE;
    transition: background-color 0.2s;
  }

  
  ${Media.phone`
    font-size: 10px;
  `}
`;

export default ItemSuggestionStyle;
