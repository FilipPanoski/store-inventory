import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const SelectedItemStyle = styled.div`
  .search-container {
    padding: 10px;
     border: 1px solid #cccccc;
     border-radius: 10px;
     
     background-color: #ffffff;
  }

  .product {
    font-family: 'Helvetica Neue', sans-serif;
    line-height: 22px;
    
    &--name {
      font-size: 16px;
      color: #FD930C;

      ${Media.desktop`
        font-size: 14px;
      `}
      ${Media.tablet`      
        font-size: 12px;
      `}
      ${Media.phone`
        font-size: 10px;
      `}
    }
    
    &--id {
      font-size: 16px;
      margin: 0 30px;

      ${Media.desktop`
        font-size: 14px;
      `}
      ${Media.tablet`      
        font-size: 12px;
      `}
      ${Media.phone`
        font-size: 10px;
        margin: 0 6px;
      `}
    }
    
    &--type {
      font-size: 14px;
      color: ${props => (props.theme.darkGray)};

      ${Media.desktop`
        font-size: 12px;
      `}
      ${Media.tablet`      
        font-size: 10px;
      `}
      ${Media.phone`
        font-size: 8px;
      `}
    }
  }
  
  .discard-product {
    float: right;
    cursor: pointer;
    color: #999;

    ${Media.phone`
      position: sticky;      
      bottom: 18px;
      left: 8px;
    `}
  }
`;

export default SelectedItemStyle;
