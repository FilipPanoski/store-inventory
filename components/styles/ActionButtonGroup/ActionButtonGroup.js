import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const ActionButtonGroup = styled.div`
  display: block;
  text-align: center;
  margin-top: 50px;
  padding-bottom: 50px;

  ${Media.phone`
    margin-left: 20px;    
    padding-bottom: 50px;
  `}
`;

export default ActionButtonGroup;
