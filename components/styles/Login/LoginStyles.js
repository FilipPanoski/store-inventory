import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const LoginStyles = styled.div`
  display: block;
  width: 550px;
  margin: 0 auto;

  ${Media.desktop`

  `}

  ${Media.tablet`
    margin: 0 auto;
  `}

  ${Media.phone`
    width: 400px;
    margin: 0 auto;
  `}
`;


export default LoginStyles;
