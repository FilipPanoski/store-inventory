import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const Button = styled.button`
  width: 160px;
  height: 35px;
  line-height: 35px;

  border: none;
  border-radius: 10px;

  color: #ffffff;

  text-align: center;
  font-family: 'Helvetica Neue', sans-serif;
  font-size: 14px;
  font-weight: 400;
  line-height: 22px;
  text-transform: uppercase;

  outline: none;
  cursor: pointer;
  
  ${props => (props.primary ? `background-color: ${props.theme.orange}` : '')}
  ${props => (props.secondary ? `background-color: ${props.theme.red}` : '')}

  ${Media.desktop`
    font-size: 12px;
  `}
`;

export default Button;
