import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const Grid = styled.div`
  justify-content: space-between;

  @media (max-width: 400px) {
    display: block;
    .col {
      width: 100%;
      margin: 0 0 10px 0;
    }
  }

  .toolbar-item {
    color: #000000;

    font-family: 'Helvetica Neue', sans-serif;
    font-size: 14px;
    font-weight: 500;
    line-height: 22px;

    &__error {
      color: #ea2e2d;
    }

    ${Media.desktop`
      font-size: 12px;
    `}
    ${Media.tablet`
      display: inline-block;
      font-size: 10px;
    `}
  }

  .new-item {
    float: right;

    color: #16c171;

    font-family: 'Helvetica Neue', sans-serif;
    font-size: 14px;
    font-weight: 500;
    
    cursor: pointer;

    ${Media.desktop`
      font-size: 12px;
    `}
    ${Media.phone`
      font-size: 10px;
      float: left;
    `}

    .fa-plus { 
      ${Media.phone`
        width: 10px !important;
        height: 10px !important;
        float: left;
      `}
    }
  }
`;

export default Grid;
