import styled from 'styled-components';

const Row = styled.div`
  width: 100%;
  flex-direction: row;

  &:last-child {
    margin-top: 40px;
  }
`;

export default Row;
