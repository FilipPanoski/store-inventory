import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const Col = styled.div`
  width: ${props => (props.default ? '33%' : '25%')};
  display: inline-flex;
  flex-direction: column;
  flex-basis: 100%;
  flex: 1;

  ${Media.phone`
    width: ${props => (props.default ? '33%' : '100%')};
    width: ${props => (props.special ? '30%' : '100%')};    
    margin: ${props => (props.default ? 'unset' : '10px auto')};
    display: ${props => (props.default ? 'inline-flex' : 'block')};
  `}

  .wrapper {
    display: block;
  }

  .control-label {
    display: inline-block;
    margin-right: 5px;

    color: #555555;

    font-size: 14px;
    font-family: 'Helvetica Neue', sans-serif;
    font-weight: 500;
    line-height: 22px;

    ${Media.desktop`
      font-size: 12px;
    `}
  }

  .no-selection::selection {
    background: transparent;
  }

  .filter-options {
    width: 30%;
    display: inline-block;
    margin-right: 15px;
    margin-bottom: 5px;
    border: none;

    color: #999999;

    font-family: 'Helvetica Neue', sans-serif;
    font-size: 14px;
    font-weight: 500;
    line-height: 22px;

    ${Media.desktop`
      font-size: 12px;
    `}
    ${Media.phone`
      width: 20%;
    `}
  }
`;

export default Col;
