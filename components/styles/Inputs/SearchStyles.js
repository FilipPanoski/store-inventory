import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const SearchStyles = styled.div`
  width: 100%;
  display: block;
  
  input {
    width: 100%;
    height: 40px;
    display: block;
    padding: 10px;
    border: 1px solid #cccccc;
    border-bottom: ${props => (props.primary ? '1px solid #cccccc' : '2px solid #fd930c')};
    border-radius: 10px;
    border-bottom-left-radius: ${props => (props.primary ? '10px' : '0')};
    border-bottom-right-radius: ${props => (props.primary ? '10px' : '0')};

    color: #000000;

    font-size: 16px;
    font-family: 'Helvetica Neue', sans-serif;
    font-weight: 500;
    line-height: 22px;

    outline: none;

    &:focus {
      outline: none;
    }

    ${Media.desktop`
      font-size: 14px;
    `}
    
    ${Media.phone`
      font-size: 10px;
    `}
  }

  label {
    display: block;

    color: #999999;
    
    font-size: 14px;
    font-family: 'Helvetica Neue', sans-serif;
    font-weight: 500;
    line-height: 22px;
    text-transform: uppercase;

    ${Media.desktop`
      font-size: 12px;
    `}
    ${Media.phone`
      font-size: 10px;
    `}
  }
  
  .search-error {
    color: ${props => (props.theme.red)};
    font-size: 13px;
  }
`;

export default SearchStyles;
