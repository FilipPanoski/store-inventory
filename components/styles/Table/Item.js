import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const Item = styled.div`
  width: 16%;
  min-width: 100px;
  max-width: 100px;
  display: table-cell;
  padding: 10px 0; 
  border-bottom: 1px solid #e5e5e5;

  color: #999999;

  font-family: 'Helvetica Neue', sans-serif;
  font-size: 14px;
  font-weight: normal;
  line-height: 22px;
  text-align: left;
  word-wrap: break-word;

  color: ${props => (props.error ? '#EF6D75' : '')};

  ${Media.desktop`
    font-size: 12px;
    padding: 8px 0;
  `}

  ${Media.tablet`
    font-size: 10px;
    padding: 6px 0;
    min-width: 80px;
    max-width: 80px;
  `}

  ${Media.phone`
    font-size: 10px;
    padding: 4px 0;
    min-width: 40px;
    max-width: 40px;
  `}
`;

export default Item;
