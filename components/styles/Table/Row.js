import styled from 'styled-components';

const Row = styled.div`
  width: 100%;
  display: table-row;  
  text-align: left;
  cursor: ${props => (props.error ? 'pointer' : 'default')};
`;

export default Row;
