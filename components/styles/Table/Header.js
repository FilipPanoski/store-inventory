import styled from 'styled-components';

const Header = styled.div`
  width: 100%;
  display: table;
  margin-bottom: 15px;
  color: #999999;
  font-family: 'Helvetica Neue', sans-serif;
  font-size: 18px;
  font-weight: normal;
  line-height: 22px;
  text-align: left;
`;

export default Header;
