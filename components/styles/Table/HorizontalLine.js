import styled from 'styled-components';

const HorizontalLine = styled.div`
  width: 1285px;
  
  border: ${props => `1px solid ${props.theme.orange}`}
`;

export default HorizontalLine;
