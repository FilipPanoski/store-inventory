import styled from 'styled-components';

const TableListDataStyles = styled.div`
  width: 100%;
  display: table;
  background: #ffffff;
`;

export default TableListDataStyles;
