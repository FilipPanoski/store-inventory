import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const ColumnName = styled.span`
  width: 16%;
  min-width: 100px;
  max-width: 100px;
  display: table-cell;

  color: #999999;

  font-family: 'Helvetica Neue', sans-serif;
  font-size: 18px;
  font-weight: normal;
  line-height: 22px;  

  ${Media.desktop`
    font-size: 16px;
  `}

  ${Media.tablet`
    font-size: 14px;
    min-width: 80px;
    max-width: 80px;
  `}

  ${Media.phone`
    font-size: 12px;
    min-width: 40px;
    max-width: 40px;
  `}
`;

export default ColumnName;
