import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const TableListStyles = styled.div`
  min-height: 100vh;
  width: 100%;
  margin-top: 90px;
  
  ${Media.desktop`
    width: 95%;
    margin: 70px auto;
  `}

  ${Media.phone`
    margin: 50px auto;
  `}

  .table-filter-wrapper {
    display: block;
    padding: 15px 25px 15px 25px;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background: #fff;
  }

  .filter-options {
    width: 15%;
    display: inline-block;
    padding: 7px;
    margin-left: 15px;
    margin-bottom: 5px;
    border: none;
    color: #999999;
    font-family: 'Helvetica Neue', sans-serif;
    font-size: 14px;
    font-weight: 500;
    line-height: 22px;

    ${Media.desktop`
      font-size: 12px;
    `}
  }

  .filter-icon {
    display: inline-block;
    color: #999999;
    font-family: 'Helvetica Neue', sans-serif;
    font-size: 14px;
    font-weight: normal;
    line-height: 22px;

    ${Media.desktop`
      font-size: 12px;
    `}
  }

  .divider {
    border-bottom: 0;
    border-left: 0;
    border-right: 0;
    border-top: 2px;
    border-style: solid;
    border-color: #fd930c;
  }

  .header-divider {
    border: 2px solid #f8f8f8;
  }

  .header-wrapper {
    width: 100%;
    padding: 0 25px;
    background-color: #ffffff;
  }
`;

export default TableListStyles;
