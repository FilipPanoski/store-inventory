import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const CardBody = styled.div`
  padding: 30px;

  height: ${props => (props.modal ? '600px' : 'unset')};
  overflow: ${props => (props.modal ? 'auto' : 'unset')};

  ${Media.laptop`
    height: ${props => (props.modal ? '400px' : 'unset')};
  `}
  ${Media.phone`
    height: ${props => (props.modal ? '400px' : 'unset')};
  `}

  .button-wrapper {
    display: block;
    margin: 0 auto;
    text-align: center;
    padding-top: 20px;
  }

  .save-btn {
    margin-right: 50px;
  }
`;

export default CardBody;
