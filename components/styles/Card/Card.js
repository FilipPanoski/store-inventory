import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const Card = styled.div`
  display: block;
  background-color: ${props => (props.main ? '#ffffff' : '#eeeeee')};

  border-top-left-radius: 10px;
  border-top-right-radius: 10px;

  ${Media.desktop`
    width: ${props => (props.formDesktop ? '100%' : '95%')};
    height: 600px;
    margin: 0 auto;
  `}
  
`;

export default Card;
