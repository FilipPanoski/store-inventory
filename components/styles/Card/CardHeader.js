import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const CardHeader = styled.div`
  padding: 30px;

  border-top-left-radius: 10px;
  border-top-right-radius: 10px;

  background-image: linear-gradient(to right, #fd940c, #f87010, #f04d12, #f13168, #fb2ca1, #ea2e2d);
  color: white;

  text-align: center;
  font-family: "Helvetica Neue", sans-serif;
  font-weight: 700;
  font-size: 26px;
  line-height: 22px;
  text-transform: uppercase;

  ${Media.desktop`
    padding: 20px;
    font-size: 22px;
    line-height: 19px;
  `}
  ${Media.phone`
    font-size: 16px;
    line-height: 16px;
  `}
`;

export default CardHeader;
