import styled from 'styled-components';

const ErrorMessage = styled.p`
  color: #ea2e2d;
  text-align: center;
  font-family: Helvetica Neue;
  font-size: 14px;
`;

export default ErrorMessage;
