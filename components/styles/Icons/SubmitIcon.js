import React from 'react';
import styled from 'styled-components';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons/faPaperPlane';
import Icon from './Icon';
import Media from '../Responsive/MediaQueries';

const SubmitIconStyle = styled.div`
  color: #fd930c;
  .fa-paper-plane {
    width: 120px !important;
    height: 120px !important;

    ${Media.desktop`
      width: 100px !important;
      height: 100px !important;
    `}
  }  
`;

const Submit = () => (
  <span className="icon">
    <SubmitIconStyle>
      <Icon icon={faPaperPlane} />
    </SubmitIconStyle>
  </span>
);

export default Submit;
