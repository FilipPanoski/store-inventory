import React from 'react';
import styled from 'styled-components';
import { faSave } from '@fortawesome/free-solid-svg-icons/faSave';
import Icon from './Icon';
import Media from '../Responsive/MediaQueries';

const SaveIconStyle = styled.div`
  color: #fd930c;
  .fa-save {
    width: 120px !important;
    height: 120px !important;

    ${Media.desktop`      
      width: 100px !important;
      height: 100px !important;
    `}
  }
`;

const Save = () => (
  <span className="icon">
    <SaveIconStyle>
      <Icon icon={faSave} style={{ height: 120, width: 120 }} />
    </SaveIconStyle>
  </span>
);

export default Save;
