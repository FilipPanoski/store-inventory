import React from 'react';
import { faPlus } from '@fortawesome/free-solid-svg-icons/faPlus';
import styled from 'styled-components';
import Icon from './Icon';
import Media from '../Responsive/MediaQueries';

const NewItem = styled.span`
  float: right;

  color: #16c171;

  font-family: 'Helvetica Neue', sans-serif;
  font-size: 14px;
  font-weight: 500;
  line-height: 22px;
  margin-left: 10px;

  ${Media.desktop`
    font-size: 12px;
  `}
  ${Media.phone`
    font-size: 10px;
  `}
`;

const Plus = () => (
  <NewItem>
    <Icon icon={faPlus} size={15} />
  </NewItem>
);

export default Plus;
