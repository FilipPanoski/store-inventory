import React from 'react';
import styled from 'styled-components';
import { faTimes } from '@fortawesome/free-solid-svg-icons/';
import Icon from './Icon';
import Media from '../Responsive/MediaQueries';

const TimesStyles = styled.div`    
  cursor: pointer;

  .fa-times {
    ${Media.phone`
      width: 15px !important;
      height: 15px !important;
    `}
  }
`;

const Times = () => (
  <span className="icon">
    <TimesStyles>
      <Icon icon={faTimes} size={25} />
    </TimesStyles>
  </span>
);

export default Times;
