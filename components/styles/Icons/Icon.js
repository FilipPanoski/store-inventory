import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styled from 'styled-components';

const IconStyles = styled.span`
  .svg-inline--fa {
    vertical-align: 0 !important;
  }
`;

const Icon = ({ icon, size }) => (
  <IconStyles>
    <FontAwesomeIcon
      icon={icon}
      style={size ? { height: size, width: size } : { height: 17, width: 17 }}
    />
  </IconStyles>
);

export default Icon;
