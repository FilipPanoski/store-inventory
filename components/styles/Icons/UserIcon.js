import React from 'react';
import { faUserCircle } from '@fortawesome/free-regular-svg-icons/faUserCircle';
import Icon from './Icon';

const UserIcon = () => (
  <span className="icon">
    <Icon icon={faUserCircle} size={19} />
  </span>
);

export default UserIcon;
