import React from 'react';
import styled from 'styled-components';
import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons/';
import Icon from './Icon';

const IncreaseQuantity = styled.span`
  color: #fd930c;
  cursor: pointer;

  &:hover {
    color: #ca7509;
  }
`;

const DecreaseQuantity = styled.span`
  color: #555555;
  cursor: pointer;

  &:hover {
    color: #767676;
  }
`;

const SingleArrow = ({ top, onClick }) => (
  <span className="toolbar-icon">
    {top ? (
      <IncreaseQuantity onClick={onClick}>
        <Icon icon={faAngleUp} />
      </IncreaseQuantity>
    ) : (
      <DecreaseQuantity onClick={onClick}>
        <Icon icon={faAngleDown} />
      </DecreaseQuantity>
    )}
  </span>
);

export default SingleArrow;
