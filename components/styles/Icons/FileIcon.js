import React from 'react';
import styled from 'styled-components';
import { faFileAlt } from '@fortawesome/free-solid-svg-icons/faFileAlt';
import Icon from './Icon';
import Media from '../Responsive/MediaQueries';

const FileIconStyle = styled.div`
  color: #fd930c;
  .fa-file-alt {
    width: 120px !important;
    height: 120px !important;

    ${Media.desktop`
      width: 100px !important;
      height: 100px !important;
    `}
  }
`;

const File = () => (
  <span className="icon">
    <FileIconStyle>
      <Icon icon={faFileAlt} />
    </FileIconStyle>
  </span>
);

export default File;
