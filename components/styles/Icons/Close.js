import React from 'react';
import styled from 'styled-components';
import { faTimes } from '@fortawesome/free-solid-svg-icons/faTimes';
import Icon from './Icon';

const CloseStyles = styled.div`
    position: relative;
    display: block;
    float: right;
    
    color: #2f4f4f;
    
    cursor: pointer;
`;

const Close = () => (
  <span className="icon">
    <CloseStyles>
      <Icon icon={faTimes} size={15} />
    </CloseStyles>
  </span>
);

export default Close;
