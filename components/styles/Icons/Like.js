import React from 'react';
import styled from 'styled-components';
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons/faThumbsUp';
import Icon from './Icon';

const LikeIconStyle = styled.div`
  color: #52F7AA;
  .fa-thumbs-up {
    width: 150px !important;
    height: 150px !important;
  }
`;

const Like = () => (
  <span className="icon">
    <LikeIconStyle>
      <Icon icon={faThumbsUp} />
    </LikeIconStyle>
  </span>
);

export default Like;
