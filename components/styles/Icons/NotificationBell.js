import React from 'react';
import styled from 'styled-components';
import { faBell } from '@fortawesome/free-solid-svg-icons/faBell';
import Icon from './Icon';

const NotificationBellStyle = styled.div`
  display: inline-block;
  margin-right: 10px;
`;

const NotificationBell = () => (
  <span className="icon">
    <NotificationBellStyle>
      <Icon icon={faBell} size={15} />
    </NotificationBellStyle>
  </span>
);

export default NotificationBell;
