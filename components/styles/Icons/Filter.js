import React from 'react';
import styled from 'styled-components';
import { faFilter } from '@fortawesome/free-solid-svg-icons/faFilter';
import Icon from './Icon';
import Media from '../Responsive/MediaQueries';

const FilterStyle = styled.span`
  color: ${props => props.theme.darkGray};
  font-family: 'Helvetica Neue', sans-serif;
  font-size: 14px;
  font-weight: 500;
  line-height: 22px;

  ${Media.desktop`
    font-size: 12px;
  `}
`;

const Filter = () => (
  <span className="icon">
    <FilterStyle>
      <Icon icon={faFilter} size={15} />
    </FilterStyle>
  </span>
);

export default Filter;
