import React from 'react';
import styled from 'styled-components';
import { faThumbsDown } from '@fortawesome/free-solid-svg-icons/faThumbsDown';
import Icon from './Icon';

const DislikeIconStyle = styled.div`
  color: #EB2E37;
  .fa-thumbs-down {
    width: 150px !important;
    height: 150px !important;
  }
`;

const Dislike = () => (
  <span className="icon">
    <DislikeIconStyle>
      <Icon icon={faThumbsDown} />
    </DislikeIconStyle>
  </span>
);

export default Dislike;
