import React from 'react';
import styled from 'styled-components';
import NotificationBell from '../Icons/NotificationBell';
import Close from '../Icons/Close';

const NotificationWrapperStyles = styled.div`
  position: fixed;
  width: 15%;
  right: 3%;
  bottom: 13%;

  -webkit-animation: fadeIn 3s;
  animation: fadeIn 3s;

  
  @keyframes fadeIn {
    from {opacity: 0;}
    to {opacity: 100;}
  }

  @-webkit-keyframes fadeIn { 
    from {
        opacity:0;
    }
    to {
        opacity:1;
    }
  }  
`;

const NotificationStyles = styled.div`
  position: relative;
  z-index: 1;
  display: block;

  padding: 25px;
  margin-top: 10px;

  border-radius: 10px;
  background-color: #EEEEEE;
  
  color: #2f4f4f;

  color: ${props => (props.success ? '#00ca46' : '')};
  color: ${props => (props.error ? '#EB2E37' : '')};
`;

const NotificationText = styled.span`
  font-family: 'Helvetica Neue', sans-serif;
  font-size: 16px;
  font-weight: 500;
  line-height: 22px;
`;

const Notification = () => (
  <NotificationWrapperStyles>
    <NotificationStyles success>
      <Close />
      <NotificationBell />
      <NotificationText>Message here!</NotificationText>
    </NotificationStyles>
    <NotificationStyles error>
      <Close />
      <NotificationBell />
      <NotificationText>Message here!</NotificationText>
    </NotificationStyles>
    <NotificationStyles>
      <Close />
      <NotificationBell />
      <NotificationText>Message here!</NotificationText>
    </NotificationStyles>
  </NotificationWrapperStyles>
);

export default Notification;
