import React from 'react';
import styled from 'styled-components';
import { faCheckCircle, faInfoCircle, faExclamationCircle } from '@fortawesome/free-solid-svg-icons/';
import { FormattedMessage } from 'react-intl';
import Icon from '../Icons/Icon';
import Media from '../Responsive/MediaQueries';

const Success = styled.span`
  margin-left: 5px;
  color: #999999;
  
  
  .fa-check-circle {
    vertical-align: text-top;
    color: ${props => props.theme.green};
    ${Media.desktop`
      width: 15px !important;
      height: 15px !important;
    `}
    ${Media.phone`
      width: 13px !important;
      height: 13px !important;
    `}
  }

  ${Media.desktop`
    font-size: 12px;
  `}
  ${Media.phone`
    font-size: 10px;
  `}
`;

const Error = styled.span`
  margin-left: 5px;
  color: ${props => props.theme.white};

  .fa-exclamation-circle {
    vertical-align: text-top;
    ${Media.desktop`
      width: 15px !important;
      height: 15px !important;
    `}
    ${Media.phone`
      width: 13px !important;
      height: 13px !important;
    `}
  }

  ${Media.desktop`
    font-size: 12px;
  `}
  ${Media.phone`
    font-size: 10px;
  `}
`;

const Default = styled.span`  
  margin-left: 5px;
  color: #999999;

  font-family: 'Helvetica Neue', sans-serif;
  font-size: 14px;
  font-weight: 500;
  line-height: 22px;

  .fa-info-circle {
    color: #fd930c;
    vertical-align: text-top;
    ${Media.desktop`
      width: 15px !important;
      height: 15px !important;
    `}
    ${Media.phone`
      width: 13px !important;
      height: 13px !important;
    `}
  }

  ${Media.desktop`
    font-size: 12px;
  `}
  ${Media.phone`
    font-size: 10px;
  `}
`;

const Message = styled.span`
  margin-left: 5px;  
  vertical-align: text-bottom;
`;

const renderNotification = (success, error) => {
  let message = (
    <Default>
      <Icon icon={faInfoCircle} />
      <Message><FormattedMessage id="notification.message_notScanned" /></Message>
    </Default>
  );
  if (success) {
    message = (
      <Success>
        <Icon icon={faCheckCircle} />
        <Message><FormattedMessage id="notification.message_scanned" /></Message>
      </Success>
    );
  } else if (error) {
    message = (
      <Error>
        <Icon icon={faExclamationCircle} />
        <Message><FormattedMessage id="notification.message_error" /></Message>
      </Error>
    );
  }
  return message;
};

const DescriptiveNotification = ({ success, error }) => (
  <span className="icon">
    {
      renderNotification(success, error)
    }
  </span>
);

export default DescriptiveNotification;
