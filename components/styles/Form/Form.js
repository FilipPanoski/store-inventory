import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const Form = styled.form`
  padding: ${props => (props.newItem ? '50px' : 'unset')};
  margin-left: ${props => (props.newItem ? '60px' : 'unset')};
  .form-control {
    width: ${props => (props.login ? '100%' : '80%')};
    height: ${props => (props.login ? 'unset' : '40px')};
    display: ${props => (props.login ? 'block' : 'inline-block')};
    padding: ${props => (props.login ? '15px' : '5px')};
    border: 1px solid #cccccc;

    color: #999999;

    font-size: 14px;
    font-family: 'Helvetica Neue', sans-serif;
    font-weight: 500;
    line-height: 22px;

    ${Media.desktop`
      height: 35px;
      font-size: 12px;
    `}
    ${Media.phone`
      width: 100%
      height: 30px;
      font-size: 10px;
    `}
  }

  .form-control-label {
    display: block;
    padding-bottom: ${props => (props.login ? '10px' : '0')};

    color: ${props => (props.login ? '#000000' : '#999999')};

    font-size: ${props => (props.login ? '18px' : '14px')};
    font-family: 'Helvetica Neue', sans-serif;
    font-weight: ${props => (props.login ? '700' : '500')};
    line-height: 22px;

    ${Media.desktop`
      font-size: 12px;
    `}

    ${Media.phone`
      font-size: 10px;
    `}
  }

  .select-location {
    width: 80%;
    height: 40px;
    display: block;
    padding: 7px;
    border: 1px solid #cccccc;

    color: #999999;
    font-size: 14px;
    font-family: 'Helvetica Neue', sans-serif;
    font-weight: 500;
    line-height: 22px;
    
    margin-left: ${props => (props.validated ? 'unset' : '22px')};

    ${Media.desktop`
      height: 35px;
      font-size: 12px;
    `}
    ${Media.tablet`
      margin-left: ${props => (props.validated ? 'unset' : '15px')};
    `}
    ${Media.phone`
      width: 100%;      
      height: 30px;
      font-size: 10px;
      margin-left: ${props => (props.validated ? 'unset' : 'unset')};
    `}
  }
`;

export default Form;
