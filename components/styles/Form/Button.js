import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const Button = styled.button`
  padding: 15px 70px;
  margin-top: 30px;
  border: none;

  font-family: 'Helvetica Neue', sans-serif;
  font-size: 16px;
  font-weight: 700;

  background-color: #f2f2f2;
  color: black;

  cursor: pointer;

  ${Media.desktop`
    font-size: 14px;
  `}

  ${Media.tablet`
    font-size: 12px;
    margin-top: 10px;
    padding: 15px 55px;
  `}

  ${Media.phone`
    font-size: 10px;
    margin-top: 0;
    padding: 10px 35px;
  `}
`;

export default Button;
