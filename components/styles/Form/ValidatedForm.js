import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const ValidatedFormStyle = styled.div`
  padding: ${props => (props.padding ? props.padding : '50px')};
  margin-left: 60px;

  ${Media.desktop`
    padding: ${props => (props.padding ? props.padding : '50px 2px')};
  `}

  ${Media.tablet`
      margin-left: 50px;
  `}

  ${Media.phone`
    margin-left: 40px;
    padding: 30px 2px 0;
  `}
  
  .form-control {
    width: ${props => (props.width ? props.width : '80%')};
    height: ${props => (props.height ? props.height : '40px')};
    padding: 5px;
    border: 1px solid #cccccc;

    color: #999999;

    font-size: 14px;
    font-family: 'Helvetica Neue', sans-serif;
    font-weight: 500;
    line-height: 22px;

    ${Media.desktop`
      height: 35px;
      font-size: 12px;
    `}
    ${Media.phone`
      height: 30px;
      font-size: 10px;
    `}
    
    &:disabled {
      background-color: rgb(235, 235, 228);
    }
  }

  .form-control-label {
    display: block;

    color: ${props => (props.theme.darkGray)};

    font-size: ${props => (props.fontSize ? props.fontSize : '14px')};
    font-family: 'Helvetica Neue', sans-serif;
    font-weight: ${props => (props.fontWeight ? props.fontWeight : '500')};
    line-height: 22px;

    ${Media.desktop`
      font-size: 12px;
    `}
    ${Media.phone`
      font-size: 10px;
    `}
  }
  
  input::-webkit-input-placeholder {
    color: rgba(0, 0, 0, 0.4) !important;
    font-size: 12px !important;
  }
     
    input:-moz-placeholder { /* Firefox 18- */
    color: rgba(0, 0, 0, 0.4) !important;
    font-size: 12px !important;  
  }
     
    input::-moz-placeholder {  /* Firefox 19+ */
    color: rgba(0, 0, 0, 0.4) !important;
    font-size: 12px !important;  
  }
     
    input:-ms-input-placeholder {  
    color: rgba(0, 0, 0, 0.4) !important;
    font-size: 12px !important;  
  }

  .select-location {
    width: 80%;
    height: 40px;
    display: block;
    padding: 7px;
    border: 1px solid #cccccc;

    color: #999999;
    font-size: 14px;
    font-family: 'Helvetica Neue', sans-serif;
    font-weight: 500;
    line-height: 22px;

    ${Media.desktop`
      height: 35px;
      font-size: 12px;
    `}
    ${Media.phone`
      width: ${props => (props.session ? '100%' : '80%')};
      height: 30px;
      font-size: 10px;
    `}

    &:disabled {
      background-color: rgb(235, 235, 228);
    }
  }
  
  .validation-error {
    color: ${props => (props.theme.red)};
  }
  
  .is-invalid-input {
    background-color: rgba(250, 0, 0, 0.2);
    border-color: red;
  }
  
  .buttons-wrapper {
    text-align: center;
    margin-top: 150px;

    ${Media.tablet`
      margin-top: 90px;
    `}

    ${Media.phone`
      margin-top: 60px;
    `}
  }
  
  button {
    margin-right: 30px;
  }
`;

export default ValidatedFormStyle;
