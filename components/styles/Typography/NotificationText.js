import styled from 'styled-components';

const NotificationText = styled.div`
  font-size: 14px;
  font-family: Helvetica Neue Medium;
  
  text-align: center;
  
  color: ${props => props.theme.red};
`;

export default NotificationText;
