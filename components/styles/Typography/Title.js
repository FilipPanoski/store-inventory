import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const Title = styled.h1`
  font-family: "Helvetica Neue", sans-serif;
  font-weight: 700;
  font-size: 22px;
  text-transform: uppercase;
  margin-bottom: 50px;

  ${Media.desktop`
    font-size: 20px;
  `}

  ${Media.tablet`
    font-size: 18px;
  `}

  ${Media.phone`
    font-size: 16px;
    margin-bottom: 20px;
  `}

  ${props => props.textCenter && 'text-align: center'}
`;

export default Title;
