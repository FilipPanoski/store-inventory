import styled from 'styled-components';

const Column = styled.div`
  font-family: Helvetica Neue;
  color: ${props => props.theme.darkGray};

  ${props => (props.item ? 'font-size: 17px' : 'font-size: 22px')}
`;

export default Column;
