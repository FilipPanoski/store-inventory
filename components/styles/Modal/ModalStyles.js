import styled from 'styled-components';
import Modal from 'react-modal';
import Media from '../Responsive/MediaQueries';

const ModalStyles = styled(Modal)`
  
`;
ModalStyles.displayName = 'StyledModal';

export const Container = styled.div`  
  background: #eeeeee;
  border: none;
  border-radius: 10px;
  height: 75%;
  margin: 150px auto;
  padding: 0;
  width: 50%;

  ${Media.tablet`
    width: 70%;
    height: 50%;
  `}
  ${Media.phone`
    width: 90%;
    height: unset;
    margin: 20px auto;
  `}
`;
Container.displayName = 'Container';

export default ModalStyles;
