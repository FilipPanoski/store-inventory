import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const DialogContent = styled.div`
  display: block;  
  padding-top: ${props => (props.save ? '10px' : '50px')};
  text-align: center;

  .dialog-warning-question {
    padding-top: 30px;

    color: #fd930c;

    font-family: "Helvetica Neue", sans-serif;
    font-weight: 500;
    font-size: 18px;
    line-height: 22px;

    ${Media.desktop`
      font-size: 16px;
    `}
  }

  .dialog-text {
    padding-top: 24px;

    color: #000000;

    font-family: "Helvetica Neue", sans-serif;
    font-weight: normal;
    font-size: 16px;
    line-height: 22px;

    &--success {
      font-size: 20px;
      font-weight: 500;

      ${Media.desktop`
        font-size: 18px;
      `}
    }

    ${Media.desktop`
      font-size: 14px;
    `}
  }
`;

export default DialogContent;
