import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const ValidatedButtonStyle = styled.span`
  
  button {
    width: 160px;
    padding: 5px;
  
    border: none;
    border-radius: 10px;
  
    color: #ffffff;
  
    text-align: center;
    font-family: 'Helvetica Neue', sans-serif;
    font-size: 14px;
    font-weight: 400;
    line-height: 22px;
    text-transform: uppercase;
    
    cursor: pointer;
  
    outline: none;
    
    ${props => (`background-color: ${props.theme.orange}`)};

    ${Media.desktop`
      font-size: 12px;
      width: 140px;
      padding: 4px;
    `}
    ${Media.phone`
      font-size: 8px;
      width: 25%;
      padding: 2px;
    `}
  }
  
  button:disabled {
    cursor: default;
    
    color: #fff;
    background-color: #6c757d;
    border-color: #6c757d;
    opacity: .65;
  }
`;

export default ValidatedButtonStyle;
