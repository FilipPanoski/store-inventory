import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const Button = styled.button`
  width: 160px;
  display: inline-block;
  padding: 5px;
  margin-right: 20px;

  border: none;
  border-radius: 10px;

  color: #ffffff;
  background-color: #dbdbdb;

  text-align: center;
  font-family: 'Helvetica Neue', sans-serif;
  font-size: 14px;
  font-weight: 400;
  line-height: 22px;
  text-transform: uppercase;

  outline: none;
  cursor: pointer;
  background-color: ${props => (props.primary ? '#fd930c' : '')};
  background-color: ${props => (props.secondary ? '#ec2e3a' : '')};
  background-color: ${props => (props.special ? '#ffffff' : '')};
  border: ${props => (props.special ? '1px solid #000000' : '')};
  color: ${props => (props.special ? '#000000' : '')};
  font-weight: ${props => (props.special ? '700' : '')};

  ${Media.desktop`
    font-size: 12px;
    width: 140px;
    padding: 4px;
  `};

  ${Media.phone`
    font-size: 8px;
    width: 25%;
    padding: 2px;
  `}
`;

export default Button;
