import styled from 'styled-components';
import Media from '../Responsive/MediaQueries';

const ToggleButtonStyles = styled.div`
  display: ${props => (props.regular ? 'block' : 'inline-block')};
  vertical-align: ${props => (props.regular ? 'unset' : 'text-bottom')};
  margin-left: ${props => (props.regular ? 'unset' : '5px')};

  ${Media.phone`
    margin-left: unset;
  `}

  .toggle-label {
    position: relative;
    display: block;
    width: ${props => (props.regular ? '100%' : '100px')};
    height: 20px;
    cursor: pointer;

    ${Media.tablet`
      width: ${props => (props.regular ? '100%' : '50px')};
      height: ${props => (props.regular ? '20px' : '15px;')};
    `}
  }
  .toggle-label input[type='checkbox'] {
    opacity: 0;
    position: absolute;
    width: ${props => (props.regular ? '100%' : '100px')};
    height: 100%;    
  }
  .toggle-label input[type='checkbox'] + .back {
    position: absolute;
    width: ${props => (props.regular ? '100%' : '100px')};
    height: ${props => (props.regular ? '30px' : '100%')};
    border-radius: ${props => (props.regular ? '5px' : '50px')};

    background-color: ${props => (props.regular ? '#fd930c' : '#ed1c24')};
    transition: background 150ms linear;

    ${Media.tablet`    
      width: ${props => (props.regular ? '100%' : '98px')};
    `}

    ${Media.phone`
      width: 80px;
    `}

  }

  .toggle-label input[type='checkbox']:checked + .back {
    background-color: ${props => (props.regular ? '#fd930c' : '#00a651')};
  }

  .toggle-label input[type='checkbox'] + .back .toggle {
    display: block;
    position: absolute;
    top: 0.1px;
    background-color: ${props => (props.regular ? '#555555' : '#e4e4e4')};
    width: 50%;
    height: 100%;
    transition: margin 150ms linear;
    border-radius: 5px;
    padding: ${props => (props.regular ? '15px' : '5px')};

    ${Media.tablet`
      height: ${props => (props.regular ? '100%' : '105%')};
      left: ${props => (props.regular ? 'unset' : '-1px')};
    `}
  }

  .toggle-label input[type='checkbox']:checked + .back .toggle {
    margin-left: ${props => (props.regular ? '140px' : '51px')};
    color: ${props => (props.regular ? 'unset' : '#000000')};

    ${Media.desktop`
      margin-left: ${props => (props.regular ? '115px' : '51px')};
    `}

    ${Media.tablet`
      margin-left: ${props => (props.regular ? '84px' : '51px')};
    `}

    ${Media.phone`
      margin-left: ${props => (props.regular ? '42px' : '40px')};
    `}
  }

  .toggle-label .label {
    position: absolute;
    width: 50%;
    display: block;

    color: ${props => (props.regular ? '#ffffff' : '#000000')};

    line-height: 22px;
    text-align: center;
    text-transform: uppercase;
    font-size: 14px;
    font-family: 'Helvetica Neue', sans-serif;
    font-weight: 500;

    ${Media.desktop`
      font-size: 12px;
    `}

    ${Media.tablet`
      font-size: 10px;
      line-height: ${props => (props.regular ? '22px' : '15px')};
    `}

    ${Media.phone`
      font-size: 8px;
      font-weight: 700;
    `}
  }

  .toggle-label .label.on {
    left: 0;
    top: ${props => (props.regular ? '5px' : '0')};
  }

  .toggle-label .label.off {
    left: ${props => (props.regular ? '135px' : '50px')};
    top: ${props => (props.regular ? '5px' : '0')};

    ${Media.desktop`
     left: ${props => (props.regular ? '115px' : '50px')};
    `}

    ${Media.tablet`
      left: ${props => (props.regular ? '84px' : '50px')};
    `}

    ${Media.phone`
      left: ${props => (props.regular ? '41px' : '39px')};
    `}    
  }

  .toggle-label input[type='checkbox']:checked + .back .label.on {
    color: ${props => (props.regular ? '#ffffff' : '#555555')};
  }
  .toggle-label input[type='checkbox'] + .back .label.off {
    color: ${props => (props.regular ? '#ffffff' : '#555555')};
  }
  .toggle-label input[type='checkbox']:checked + .back .label.off {
    color: ${props => (props.regular ? '#e4e4e4' : '#555555')};
  }
`;

export default ToggleButtonStyles;
