import React from 'react';
import { FormattedMessage } from 'react-intl';

class LocationPicker extends React.PureComponent {
  renderLabel = name => (
    <div className="label-wrapper">
      <FormattedMessage id={name} />
    </div>
  );

  onChange = (e) => {
    const { saveLocation } = this.props;

    const customEvent = { target: { name: e.target.name, value: JSON.parse(e.target.value) } };
    saveLocation(customEvent);
  };

  render() {
    const {
      disabled, locations, location, handleDropdownKeyDown, name,
      withLabel,
    } = this.props;

    return (
      <label className="form-control-label" htmlFor={name}>
        {withLabel ? this.renderLabel(name) : ''}
        <select
          className="select-location"
          disabled={disabled}
          name={name}
          value={JSON.stringify(location)}
          onKeyDown={e => handleDropdownKeyDown(e)}
          onChange={e => this.onChange(e)}
        >
          {locations.map(loc => (
            <option value={JSON.stringify(loc)} key={loc.id}>
              {loc.locationName}
            </option>
          ))}
        </select>
      </label>
    );
  }
}

export default LocationPicker;
