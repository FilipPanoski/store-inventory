import React from 'react';
import { FormattedMessage } from 'react-intl';
import Times from './styles/Icons/Times';
import SelectedItemStyle from './styles/Outputs/SelectedItemStyle';
import Label from './Label';

const SelectedItem = ({
  deselectItem, foundItem, id, isSerialized, name, productId, productName,
}) => (
  <SelectedItemStyle>
    <Label
      id={id}
      name={name}
      component={(
        <div className="search-container">
          <span className="product product--name">{productName}</span>
          <span className="product product--id">{productId}</span>
          <span className="product product--type">
            {isSerialized ? <FormattedMessage id="serialized" /> : <FormattedMessage id="nonSerialized" />}
          </span>
          {
            foundItem ? null
              : (
                <span
                  tabIndex={0}
                  role="button"
                  className="discard-product"
                  onKeyPress={deselectItem}
                  onClick={deselectItem}
                >
                  <Times />
                </span>
              )
          }
        </div>
)}
    />
  </SelectedItemStyle>
);

export default SelectedItem;
