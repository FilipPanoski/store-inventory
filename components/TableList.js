import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import memoize from 'memoize-one';
import Filter from './styles/Icons/Filter';
import { SERIALIZED, NON_SERIALIZED } from '../utils/inventoryItemTypes';
import {
  TableListStyles, TableListDataStyles, Header, Row, ColumnName, Item,
} from './styles/Table/index';

class TableList extends Component {
  filter = memoize(
    (data, filterBy, dataFilter) => data.filter(item => item[filterBy] === dataFilter),
  );

  constructor(props) {
    super(props);

    this.state = {
      data: props.data,
      dataFilter: '',
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const newLastItem = nextProps.data[0];
    const prevNewItem = prevState.data[0];

    if (newLastItem.key !== prevNewItem.key) {
      if (newLastItem.inventoryItemType !== prevState.dataFilter) {
        return { data: nextProps.data, dataFilter: '' };
      }
      return { data: nextProps.data };
    }
    return null;
  }

  changeDataFilter = (e) => {
    this.setState({ dataFilter: e.target.value });
  };

  filterTable = () => {
    const { filterBy, data } = this.props;
    const { dataFilter } = this.state;

    if (!dataFilter) {
      return data;
    }

    return this.filter(data, filterBy, dataFilter);
  };

  render() {
    const { dataFilter } = this.state;
    const { columns, handleItemClick } = this.props;
    const columnKeys = columns.map(column => column.key);
    const tableData = this.filterTable();

    return (
      <TableListStyles>
        <div className="table-filter-wrapper">
          <Filter />
          <select className="filter-options" value={dataFilter} onChange={this.changeDataFilter}>
            <option value="">All</option>
            <option value={SERIALIZED}>Serialized</option>
            <option value={NON_SERIALIZED}>Non-serialized</option>
          </select>
          <hr className="divider" />
        </div>
        <div className="header-wrapper">
          <Header>
            <Row error={null}>
              {columns.map(column => (
                <ColumnName key={column.key}><FormattedMessage id={`table.header.label.${column.key}`} /></ColumnName>
              ))}
            </Row>
          </Header>
          <hr className="header-divider" />
          <TableListDataStyles>
            {tableData
              && tableData.map(element => (
                <Row
                  key={element.key}
                  error={element.error}
                  onClick={() => handleItemClick(element)}
                >
                  {columnKeys.map(columnKey => (
                    <Item
                      key={columnKey}
                      error={element.error}
                    >
                      {element[columnKey] && element[columnKey].value}
                    </Item>
                  ))}
                </Row>
              ))}
          </TableListDataStyles>
        </div>
      </TableListStyles>
    );
  }
}

export default TableList;
