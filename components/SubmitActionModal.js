import React from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Card, CardBody, CardHeader } from './styles/Card';
import ActionButtonGroup from './styles/ActionButtonGroup/ActionButtonGroup';
import Button from './styles/Button/Button';
import { toggleDialog } from '../store/actions/dialog';
import DialogContent from './styles/Modal/DialogContent';
import Submit from './styles/Icons/SubmitIcon';
import { submitToErp } from '../store/actions/erpSubmit';
import { ErrorMessage } from './styles/ErrorMessage';
import { ERROR_SUBMIT } from '../utils/submitSessionStatus';

export class SubmitActionModal extends React.PureComponent {
  onSubmit = () => {
    const { onErpSubmit, sessionId } = this.props;
    onErpSubmit(sessionId);
  };

  onCancel = () => {
    const { onToggleDialog } = this.props;
    onToggleDialog();
  };

  render() {
    const {
      erpStatus,
    } = this.props;

    return (
      <Card formDesktop>
        <CardHeader><FormattedMessage id="submit.modal.header" /></CardHeader>
        <CardBody>
          <DialogContent>
            <Submit />
            <div className="dialog-warning-question"><FormattedMessage id="submit.modal.dialog_question" /></div>
            <div className="dialog-text"><FormattedMessage id="submit.modal.dialog_text" /></div>
          </DialogContent>
        </CardBody>
        {
          (erpStatus === ERROR_SUBMIT)
            ? <ErrorMessage><FormattedMessage id="submit.modal.dialog_error" /></ErrorMessage>
            : null
        }
        <ActionButtonGroup>
          <Button primary type="button" onClick={() => this.onSubmit()}>Submit</Button>
          <Button type="button" onClick={() => this.onCancel()}>Cancel</Button>
        </ActionButtonGroup>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  erpStatus: state.erpSubmit.data,
  sessionId: state.storeSession.id,
});

const mapActionsToProps = {
  onErpSubmit: submitToErp,
  onToggleDialog: toggleDialog,
};

export default connect(mapStateToProps, mapActionsToProps)(SubmitActionModal);
