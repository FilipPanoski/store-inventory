import React from 'react';
import ItemSuggestionStyle from './styles/Outputs/ItemSuggestionStyle';

const ItemSuggestion = ({ suggestions, onSelectItem }) => (
  <div>
    {
      suggestions.map(suggestion => (
        <ItemSuggestionStyle key={suggestion.productId} onClick={() => onSelectItem(suggestion)}>
          <span className="suggestion-name">{suggestion.productName}</span>
          <span className="suggestion-id">{suggestion.productId}</span>
        </ItemSuggestionStyle>
      ))
    }
  </div>
);

export default ItemSuggestion;
