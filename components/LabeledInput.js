import React from 'react';
import Label from './Label';

const LabeledInput = ({
  disabled, type, id, name, placeholder, value, onChange,
}) => (
  <span>
    <Label
      id={id}
      name={name}
      component={(
        <input
          disabled={disabled}
          type={type}
          className="form-control"
          id={id}
          name={name}
          placeholder={placeholder}
          value={value}
          onChange={onChange}
        />
)}
    />
  </span>
);

export default LabeledInput;
