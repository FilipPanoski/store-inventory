import { put, fork, takeLatest } from 'redux-saga/effects';
import es6promise from 'es6-promise';
import 'isomorphic-unfetch';

import {
  InventoryItemsActions,
  existingItemSaveSuccess,
  existingItemSaveFail,
  newItemSaveFail,
  newItemSaveSuccess,
  existingItemPutFail,
  existingItemPutSuccess,
  newItemEditFail,
  newItemEditSuccess,
} from '../actions/inventoryItems';

es6promise.polyfill();

function* saveExistingInventoryItem({ payload }) {
  try {
    const response = yield fetch(`${process.env.PROXY_SERVER_ENDPOINT}/api/saveScannedItem`, {
      body: JSON.stringify(payload),
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      method: 'POST',
    });
    const data = yield response.json();
    data.lastScannedLocation = payload.inventoryLocation;
    yield put(existingItemSaveSuccess(data));
  } catch (error) {
    console.error(error);//eslint-disable-line
    yield put(existingItemSaveFail(error));
  }
}

function* putExistingInventoryItem({ payload }) {
  try {
    const response = yield fetch(`${process.env.PROXY_SERVER_ENDPOINT}/api/inventoryItem`, {
      body: JSON.stringify(payload),
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      method: 'PUT',
    });
    const data = yield response.json();
    data.inventoryLocation = payload.inventoryLocation;
    yield put(existingItemPutSuccess(data));
  } catch (error) {
    console.error(error);
    yield put(existingItemPutFail(error));
  }
}

function* saveNewInventoryItem({ payload }) {
  try {
    const response = yield fetch(`${process.env.PROXY_SERVER_ENDPOINT}/api/inventoryItem`, {
      body: JSON.stringify(payload),
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      method: 'POST',
    });
    const data = yield response.json();
    if (data) {
      yield put(newItemSaveSuccess({
        ...payload,
        historyOfScannedQuantities: data.historyOfScannedQuantities,
        id: data.id,
        inventoryItemType: data.inventoryItemType,
        lastScanDateTime: data.lastScanDateTime,
        serializedItem: data.serializedItem,
        status: data.status,
        totalQuantity: data.totalQuantity,
      }));
    } else {
      yield put(newItemSaveFail(data));
    }
  } catch (e) {
    console.error(e);//eslint-disable-line
  }
}

function* editNewInventoryItem({ payload }) {
  try {
    const response = yield fetch(`${process.env.PROXY_SERVER_ENDPOINT}/api/editNewInventoryItem`, {
      body: JSON.stringify(payload),
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      method: 'PUT',
    });
    const data = yield response.json();

    if (data) {
      yield put(newItemEditSuccess({
        ...payload,
        historyOfScannedQuantities: data.historyOfScannedQuantities,
        id: data.id,
        inventoryItemType: data.inventoryItemType,
        lastScanDateTime: data.lastScanDateTime,
        serializedItem: data.serializedItem,
        status: data.status,
        totalQuantity: data.totalQuantity,
      }));
    } else {
      yield put(newItemEditFail(data));
    }
  } catch (e) {
    console.error(e);
  }
}

export default [
  fork(takeLatest, InventoryItemsActions.EXISTING_ITEM_PUT, putExistingInventoryItem),
  fork(takeLatest, InventoryItemsActions.EXISTING_ITEM_SAVE, saveExistingInventoryItem),
  fork(takeLatest, InventoryItemsActions.NEW_ITEM_SAVE, saveNewInventoryItem),
  fork(takeLatest, InventoryItemsActions.NEW_ITEM_EDIT, editNewInventoryItem),
];
