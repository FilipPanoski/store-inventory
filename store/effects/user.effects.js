import {
  call, fork, put, takeLatest,
} from 'redux-saga/effects';
import es6promise from 'es6-promise';
import 'isomorphic-unfetch';

import { fetchUserFail, fetchUserSuccess, UserActions } from '../actions/user';

es6promise.polyfill();

function fetchUserFromApi(userId = process.env.DEFAULT_USER, treCookie) {
  return fetch(`${process.env.PROXY_SERVER_ENDPOINT}/api/user/?userId=${userId}`, { headers: { cookie: treCookie }, method: 'GET' });
}

function* fetchUser({ payload }) {
  try {
    const { userId, cookie } = payload;
    const response = yield call(fetchUserFromApi, userId, cookie);
    const data = yield response.json();

    if (data.status) {
      yield put(fetchUserFail(data));
    } else {
      yield put(fetchUserSuccess(data));
    }
  } catch (e) {
    console.error(e);
  }
}

export default [fork(takeLatest, UserActions.FETCH_USER, fetchUser)];
