import { all } from 'redux-saga/effects';

import erpEffects from './erpSubmit.effects';
import userEffects from './user.effects';
import storeSessionEffects from './storeSession.effects';
import inventoryItemsEffects from './inventoryItems.effects';
import productCatalogue from './productCatalogue.effects';

export default function* rootSaga() {
  yield all([
    ...userEffects, ...storeSessionEffects, ...productCatalogue, ...inventoryItemsEffects,
    ...erpEffects,
  ]);
}
