import 'es6-promise';
import 'isomorphic-unfetch';
import { fork, put, takeLatest } from 'redux-saga/effects';
import {
  ErpActions,
  submitToErpFail,
  submitToErpSuccess,
  submitToErpStatus,
} from '../actions/erpSubmit';
import { SUBMITTING, SUBMITTED } from '../../utils/submitSessionStatus';

function* erpSubmit(payload) {
  try {
    const response = yield fetch(`${process.env.PROXY_SERVER_ENDPOINT}/api/submitToErp`, {
      body: JSON.stringify(payload),
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      method: 'POST',
    });
    let data = null;
    if (response) {
      data = JSON.parse(yield response.json());
    }

    if (data === null || !data.status) {
      yield put(submitToErpStatus(payload.sessionId));
    } else {
      yield put(submitToErpFail(data));
    }
  } catch (e) {
    console.error(e);
  }
}

function* checkStatus(payload) {
  try {
    const response = yield fetch(`${process.env.PROXY_SERVER_ENDPOINT}/api/checkErpStatus`, {
      body: JSON.stringify(payload),
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      method: 'POST',
    });

    const data = JSON.parse(yield response.json());

    switch (data) {
      case SUBMITTING: {
        yield put(submitToErpStatus(payload.sessionId));
        break;
      }
      case SUBMITTED: {
        yield put(submitToErpSuccess(data));
        break;
      }
      default: {
        yield put(submitToErpFail(data));
        break;
      }
    }
  } catch (e) {
    console.error(e);
  }
}

function submitToErpSuccessSaga() {
  window.location.href = '/login';
}

export default [
  fork(takeLatest, ErpActions.SUBMIT_TO_ERP, erpSubmit),
  fork(takeLatest, ErpActions.SUBMIT_TO_ERP_STATUS, checkStatus),
  fork(takeLatest, ErpActions.SUBMIT_TO_ERP_SUCCESS, submitToErpSuccessSaga),
];
