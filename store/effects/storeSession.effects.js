import 'es6-promise';
import 'isomorphic-unfetch';
import { fork, put, takeLatest } from 'redux-saga/effects';
import Router from 'next/router';
import {
  startSessionFail,
  startSessionSuccess,
  StoreSessionActions,
} from '../actions/storeSession';

function* startSession({ payload }) {
  try {
    const {
      store, user, cookie, isLoading,
    } = payload;
    const { storeNumber, storeName } = store;

    if (!storeNumber || !user) return;

    const response = yield fetch(
      `${process.env.PROXY_SERVER_ENDPOINT}/api/loadSession/${user.userId}/${
        user.name
      }/${storeNumber}/${storeName}/${user.country}`,
      {
        headers: { Cookie: cookie },
      },
    );

    const data = yield response.json();
    data.isLoading = isLoading;

    if (data.status) {
      yield put(startSessionFail(data));
    } else {
      yield put(startSessionSuccess(data));
    }
  } catch (e) {
    console.error(e);//eslint-disable-line
  }
}

function startSessionSuccessSaga() {
  Router.push('/');
}

export default [
  fork(takeLatest, StoreSessionActions.START_SESSION, startSession),
  fork(takeLatest, StoreSessionActions.START_SESSION_SUCCESS, startSessionSuccessSaga),
];
