import 'es6-promise';
import 'isomorphic-unfetch';
import { fork, put, takeLatest } from 'redux-saga/effects';
import {
  getProductCatalogueFail,
  getProductCatalogueSuccess,
  ProductCatalogueActions,
} from '../actions/productCatalogue';

function* getProductCatalogue() {
  try {
    const response = yield fetch(`${process.env.PROXY_SERVER_ENDPOINT}/api/getProductCatalogue`);
    const data = yield response.json();

    if (data.status) {
      yield put(getProductCatalogueFail(data));
    } else {
      yield put(getProductCatalogueSuccess(data));
    }
  } catch (e) {
    console.error(e);//eslint-disable-line
  }
}

export default [
  fork(takeLatest, ProductCatalogueActions.GET_PRODUCT_CATALOGUE, getProductCatalogue),
];
