import { StoreSessionActions } from '../actions/storeSession';

const initialState = {
  error: null,
  list: null,
};

export default function inventoryLocationsReducer(state = initialState, action) {
  switch (action.type) {
    case StoreSessionActions.START_SESSION_SUCCESS: {
      return {
        ...state,
        error: null,
        list: action.payload.inventoryLocations,
      };
    }

    case StoreSessionActions.START_SESSION_FAIL: {
      return {
        error: action.error,
        list: null,
      };
    }

    default: {
      return state;
    }
  }
}
