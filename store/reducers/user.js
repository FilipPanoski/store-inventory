import { UserActions } from '../actions/user';

const initialState = {
  entity: null,
  loaded: false,
  loading: false,
};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case UserActions.FETCH_USER: {
      return {
        ...state,
        loading: true,
      };
    }

    case UserActions.FETCH_USER_SUCCESS: {
      return {
        ...state,
        entity: action.payload,
        loaded: true,
        loading: false,
      };
    }

    default: {
      return state;
    }
  }
}
