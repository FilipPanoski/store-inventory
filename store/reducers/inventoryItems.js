import { StoreSessionActions } from '../actions/storeSession';
import { InventoryItemsActions } from '../actions/inventoryItems';

const initialState = {
  error: null,
  list: null,
};

export default function inventoryItemsReducer(state = initialState, action) {
  switch (action.type) {
    case StoreSessionActions.START_SESSION_SUCCESS: {
      return {
        ...state,
        error: null,
        list: action.payload.inventoryItems,
      };
    }

    case StoreSessionActions.START_SESSION_FAIL: {
      return {
        error: action.error,
        list: null,
      };
    }

    case InventoryItemsActions.EXISTING_ITEM_PUT_SUCCESS: {
      return {
        ...state,
        list: [
          {
            ...state.list.filter(item => item.id === action.payload.id)[0],
            historyOfScannedQuantities: action.payload.historyOfScannedQuantities,
            inventoryLocation: action.payload.inventoryLocation,
            status: action.payload.status,
            totalQuantity: action.payload.totalQuantity,
          },
          ...state.list.filter(item => item.id !== action.payload.id),
        ],
      };
    }

    case InventoryItemsActions.EXISTING_ITEM_SAVE_SUCCESS: {
      return {
        ...state,
        list: [
          {
            ...state.list.filter(item => item.id === action.payload.id)[0],
            historyOfScannedQuantities: action.payload.historyOfScannedQuantities,
            lastScannedLocation: action.payload.lastScannedLocation,
            status: action.payload.status,
            totalQuantity: action.payload.totalQuantity,
          },
          ...state.list.filter(item => item.id !== action.payload.id),
        ],
      };
    }

    case InventoryItemsActions.NEW_ITEM_SAVE_SUCCESS: {
      return {
        ...state,
        list: [action.payload].concat(state.list),
      };
    }

    case InventoryItemsActions.NEW_ITEM_EDIT_SUCCESS: {
      return {
        ...state,
        list: [
          {
            ...state.list.filter(item => item.id === action.payload.id)[0],
            ean: action.payload.ean,
            historyOfScannedQuantities: action.payload.historyOfScannedQuantities,
            imei: action.payload.imei,
            inventoryItemType: action.payload.inventoryItemType,
            inventoryLocation: action.payload.inventoryLocation,
            productId: action.payload.productId,
            productName: action.payload.productName,
            status: action.payload.status,
            totalQuantity: action.payload.totalQuantity,
          },
          ...state.list.filter(item => item.id !== action.payload.id),
        ],
      };
    }

    default: {
      return state;
    }
  }
}
