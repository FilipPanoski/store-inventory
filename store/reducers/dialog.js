import { DialogActions } from '../actions/dialog';

const initialState = {
  dialogProps: {},
  dialogType: '',
  isDialogOpen: false,
};

export default function dialogReducer(state = initialState, action) {
  switch (action.type) {
    case DialogActions.TOGGLE_DIALOG:
      return {
        ...state,
        dialogProps: action.payload.props,
        dialogType: action.payload.type,
        isDialogOpen: !state.isDialogOpen,
      };
    default: {
      return state;
    }
  }
}
