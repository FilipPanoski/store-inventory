import { combineReducers } from 'redux';
import userReducer from './user';
import storeSessionReducer from './storeSession';
import inventoryLocationsReducer from './inventoryLocations';
import inventoryItemsReducer from './inventoryItems';
import dialogReducer from './dialog';
import productCatalogueReducer from './productCatalogue';
import erpReducer from './erpSubmit';

export default combineReducers({
  dialog: dialogReducer,
  erpSubmit: erpReducer,
  inventoryItems: inventoryItemsReducer,
  inventoryLocations: inventoryLocationsReducer,
  productCatalogue: productCatalogueReducer,
  storeSession: storeSessionReducer,
  user: userReducer,
});
