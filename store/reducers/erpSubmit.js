import { ErpActions } from '../actions/erpSubmit';

const initialState = {
  data: null,
  isLoading: false,
  isSubmitting: true,
  successfulSubmit: false,
  userId: null,
};

export default function dialogReducer(state = initialState, action) {
  switch (action.type) {
    case ErpActions.SUBMIT_TO_ERP:
      return {
        ...state,
        isLoading: true,
      };
    case ErpActions.SUBMIT_TO_ERP_FAIL:
      return {
        ...state,
        data: action.payload,
        isLoading: false,
        isSubmitting: false,
        successfulSubmit: false,
      };
    case ErpActions.SUBMIT_TO_ERP_STATUS:
      return {
        ...state,
        isLoading: true,
        isSubmitting: true,
        successfulSubmit: false,
      };
    case ErpActions.SUBMIT_TO_ERP_SUCCESS:
      return {
        ...state,
        successfulSubmit: true,
        userId: action.payload,
      };
    default: {
      return state;
    }
  }
}
