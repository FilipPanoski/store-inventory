import { ProductCatalogueActions } from '../actions/productCatalogue';

const initialState = {
  error: null,
  productCatalogue: [],
};

export default function productCatalogueReducer(state = initialState, action) {
  switch (action.type) {
    case ProductCatalogueActions.GET_PRODUCT_CATALOGUE_SUCCESS:
      return {
        ...state,
        error: null,
        productCatalogue: action.payload,
      };

    case ProductCatalogueActions.GET_PRODUCT_CATALOGUE_FAIL:
      return {
        ...state,
        error: action.error,
        productCatalogue: [],
      };

    default: {
      return state;
    }
  }
}
