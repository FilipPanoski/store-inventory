import { StoreSessionActions } from '../actions/storeSession';

const initialState = {
  error: null,
  id: null,
  isLoading: false,
  name: null,
};

export default function storeSessionReducer(state = initialState, action) {
  switch (action.type) {
    case StoreSessionActions.START_SESSION: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case StoreSessionActions.START_SESSION_SUCCESS: {
      return {
        ...state,
        error: null,
        id: action.payload.id,
        isLoading: action.payload.isLoading,
        name: action.payload.storeName,
        submitSessionStatus: action.payload.submitSessionStatus,
      };
    }

    case StoreSessionActions.START_SESSION_FAIL: {
      return {
        error: action.error,
        id: null,
        isLoading: false,
        name: null,
      };
    }

    default: {
      return state;
    }
  }
}
