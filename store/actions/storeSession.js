export const StoreSessionActions = {
  START_SESSION: '@storeSession/start',
  START_SESSION_FAIL: '@storeSession/start/fail',
  START_SESSION_SUCCESS: '@storeSession/start/success',
};

export function startSession(store, user, cookie, isLoading) {
  return {
    payload: {
      cookie, isLoading, store, user,
    },
    type: StoreSessionActions.START_SESSION,
  };
}

export function startSessionSuccess(payload) {
  return {
    payload,
    type: StoreSessionActions.START_SESSION_SUCCESS,
  };
}

export function startSessionFail(error) {
  return {
    error,
    type: StoreSessionActions.START_SESSION_FAIL,
  };
}
