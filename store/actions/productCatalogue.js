export const ProductCatalogueActions = {
  GET_PRODUCT_CATALOGUE: '@productCatalogue/get',
  GET_PRODUCT_CATALOGUE_FAIL: '@productCatalogue/get/fail',
  GET_PRODUCT_CATALOGUE_SUCCESS: '@productCatalogue/get/success',
};

export function getProductCatalogue() {
  return {
    type: ProductCatalogueActions.GET_PRODUCT_CATALOGUE,
  };
}

export function getProductCatalogueSuccess(payload) {
  return {
    payload,
    type: ProductCatalogueActions.GET_PRODUCT_CATALOGUE_SUCCESS,
  };
}

export function getProductCatalogueFail(error) {
  return {
    error,
    type: ProductCatalogueActions.GET_PRODUCT_CATALOGUE_FAIL,
  };
}
