export const ErpActions = {
  SUBMIT_TO_ERP: '@erpSubmit/submit',
  SUBMIT_TO_ERP_FAIL: '@erpSubmit/submitFail',
  SUBMIT_TO_ERP_STATUS: '@erpSUbmit/status',
  SUBMIT_TO_ERP_SUCCESS: '@erpSubmit/submitSuccess',
};

export function submitToErp(payload) {
  return {
    sessionId: payload,
    type: ErpActions.SUBMIT_TO_ERP,
  };
}

export function submitToErpStatus(payload) {
  return {
    sessionId: payload,
    type: ErpActions.SUBMIT_TO_ERP_STATUS,
  };
}

export function submitToErpFail(payload) {
  return {
    payload,
    type: ErpActions.SUBMIT_TO_ERP_FAIL,
  };
}

export function submitToErpSuccess(payload) {
  return {
    payload,
    type: ErpActions.SUBMIT_TO_ERP_SUCCESS,
  };
}
