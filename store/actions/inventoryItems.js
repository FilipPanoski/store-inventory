export const InventoryItemsActions = {
  EXISTING_ITEM_PUT: '@inventoryItem/existing/put',
  EXISTING_ITEM_PUT_FAIL: '@inventoryItem/existing/put/fail',
  EXISTING_ITEM_PUT_SUCCESS: '@inventoryItem/existing/put/success',

  EXISTING_ITEM_SAVE: '@inventoryItem/existing/save',
  EXISTING_ITEM_SAVE_FAIL: '@inventoryItem/existing/save/fail',
  EXISTING_ITEM_SAVE_SUCCESS: '@inventoryItem/existing/save/success',

  NEW_ITEM_EDIT: '@inventoryItem/new/edit',
  NEW_ITEM_EDIT_FAIL: '@inventoryItem/new/edit/fail',
  NEW_ITEM_EDIT_SUCCESS: '@inventoryItem/new/edit/success',

  NEW_ITEM_SAVE: '@inventoryItem/new/save',
  NEW_ITEM_SAVE_FAIL: '@inventoryItem/new/save/fail',
  NEW_ITEM_SAVE_SUCCESS: '@inventoryItem/new/save/success',
};

export function existingItemPut(item) {
  return {
    payload: item,
    type: InventoryItemsActions.EXISTING_ITEM_PUT,
  };
}

export function existingItemPutFail(item) {
  return {
    payload: item,
    type: InventoryItemsActions.EXISTING_ITEM_PUT_FAIL,
  };
}

export function existingItemPutSuccess(item) {
  return {
    payload: item,
    type: InventoryItemsActions.EXISTING_ITEM_PUT_SUCCESS,
  };
}

export function existingItemSave(item) {
  return {
    payload: item,
    type: InventoryItemsActions.EXISTING_ITEM_SAVE,
  };
}

export function existingItemSaveSuccess(payload) {
  return {
    payload,
    type: InventoryItemsActions.EXISTING_ITEM_SAVE_SUCCESS,
  };
}

export function existingItemSaveFail(error) {
  return {
    error,
    type: InventoryItemsActions.EXISTING_ITEM_SAVE_FAIL,
  };
}

export function newItemEdit(item) {
  return {
    payload: item,
    type: InventoryItemsActions.NEW_ITEM_EDIT,
  };
}

export function newItemEditSuccess(item) {
  return {
    payload: item,
    type: InventoryItemsActions.NEW_ITEM_EDIT_SUCCESS,
  };
}

export function newItemEditFail(error) {
  return {
    error,
    type: InventoryItemsActions.NEW_ITEM_EDIT_FAIL,
  };
}

export function newItemSave(item) {
  return {
    payload: item,
    type: InventoryItemsActions.NEW_ITEM_SAVE,
  };
}

export function newItemSaveSuccess(item) {
  return {
    payload: item,
    type: InventoryItemsActions.NEW_ITEM_SAVE_SUCCESS,
  };
}

export function newItemSaveFail(error) {
  return {
    error,
    type: InventoryItemsActions.NEW_ITEM_SAVE_FAIL,
  };
}
