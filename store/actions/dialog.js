export const DialogActions = {
  TOGGLE_DIALOG: '@dialog/toggle',
};

export function toggleDialog(type, props) {
  return {
    payload: { props, type },
    type: DialogActions.TOGGLE_DIALOG,
  };
}
