export const UserActions = {
  FETCH_USER: '@user/fetch',
  FETCH_USER_FAIL: '@user/fetch/fail',
  FETCH_USER_SUCCESS: '@user/fetch/success',
};

export function fetchUser(userId, cookie) {
  return {
    payload: { cookie, userId },
    type: UserActions.FETCH_USER,
  };
}

export function fetchUserSuccess(payload) {
  return {
    payload,
    type: UserActions.FETCH_USER_SUCCESS,
  };
}

export function fetchUserFail(error) {
  return {
    error,
    type: UserActions.FETCH_USER_FAIL,
  };
}
