const express = require('express');
const { json, raw } = require('body-parser');

const router = express.Router();

router.use(json());
router.use(raw({ type: () => true }));

// Load the `/api/user` endpoints
router.use('/user', require('./user'));
router.use('/loadSession', require('./storeSession'));
router.use('/saveScannedItem', require('./saveScannedItem'));
router.use('/inventoryItem', require('./inventoryItem'));
router.use('/editNewInventoryItem', require('./editNewInventoryItem'));
router.use('/getProductCatalogue', require('./productCatalogue'));
router.use('/submitToErp', require('./erpSubmit'));
router.use('/checkErpStatus', require('./checkErpStatus'));

module.exports = router;
