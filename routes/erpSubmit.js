const express = require('express');
const fetch = require('isomorphic-unfetch');

const router = express.Router();

router.post('/', async (req, res, next) => {
  const { sessionId } = req.body;
  try {
    const request = await fetch(`${process.env.BACKEND_SERVER_ENDPOINT}/session/submit/${sessionId}`, {
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      method: 'POST',
    });

    const response = await request;

    const data = JSON.stringify(response);
    await res.json(data);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
