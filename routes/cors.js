const cors = require('cors');

module.exports = cors({
  allowedHeaders: ['Authorization', 'Origin', 'X-Requested-With', 'Content-Type', 'Accept'],
  credentials: true,
  origin: true,
});
