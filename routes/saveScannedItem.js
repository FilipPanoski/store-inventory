const express = require('express');
const fetch = require('isomorphic-unfetch');

const router = express.Router();

router.post('/', async (req, res, next) => {
  try {
    const { body } = req;
    const headers = {
      'Content-Type': 'application/json; charset=utf-8',
    };

    const response = await fetch(`${process.env.BACKEND_SERVER_ENDPOINT}/scannedItem`, {
      body: JSON.stringify(body),
      headers,
      method: 'POST',
    });
    const data = await response.json();

    await res.json(data);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
