const express = require('express');
const fetch = require('isomorphic-unfetch');

const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    const request = await fetch(`${process.env.BACKEND_SERVER_ENDPOINT}/productCatalogue`);

    const data = await request.json();
    await res.json(data);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
