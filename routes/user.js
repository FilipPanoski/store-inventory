const express = require('express');
const fetch = require('isomorphic-unfetch');

const router = express.Router();

router.get('/:userId?', async (req, res, next) => {
  try {
    const userId = req.query.userId ? req.query.userId : process.env.DEFAULT_USER;
    const request = await fetch(`${process.env.BACKEND_SERVER_ENDPOINT}/user/${userId}`, { headers: { cookie: req.headers.cookie } });

    const data = await request.json();
    req.session.user = data;

    await res.json(data);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
