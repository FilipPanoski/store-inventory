const express = require('express');
const fetch = require('isomorphic-unfetch');

const router = express.Router();

router.get('/:userId/:userName/:storeNumber/:storeName/:country', async (req, res, next) => {
  try {
    const {
      userId, userName, storeName, storeNumber, country,
    } = req.params;

    const url = `${process.env.BACKEND_SERVER_ENDPOINT}/session/${userId}/${userName}/${storeNumber}/${storeName}/${country}`;
    const request = await fetch(url, {
      headers: { cookie: req.headers.cookie },
    });
    const data = await request.json();

    req.session.store = { storeName, storeNumber, userId };

    await res.json(data);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
