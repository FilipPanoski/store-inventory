const express = require('express');
const session = require('./session');

const router = express.Router();
const cors = require('./cors');

// router.options('/*', (req, res) => {
//   res.status(204).send('');
// });

router.use(session);

router.use(cors);

router.use('/api', require('./api'));

module.exports = router;
