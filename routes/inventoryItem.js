const express = require('express');
const fetch = require('isomorphic-unfetch');

const router = express.Router();

router.all('/', async (req, res, next) => {
  try {
    const { body } = req;
    const headers = {
      'Content-Type': 'application/json; charset=utf-8',
    };
    const request = await fetch(`${process.env.BACKEND_SERVER_ENDPOINT}/inventoryItem`, {
      body: JSON.stringify(body),
      headers,
      method: req.method,
    });
    const data = await request.json();
    await res.json(data);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
