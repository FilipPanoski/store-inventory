const express = require('express');
const fetch = require('isomorphic-unfetch');

const router = express.Router();

router.post('/', async (req, res, next) => {
  const { sessionId } = req.body;
  try {
    const request = await fetch(`${process.env.BACKEND_SERVER_ENDPOINT}/session/status/${sessionId}`);

    const response = await request.json();

    const data = JSON.stringify(response);

    if (data.status === 'SUBMITTED') {
      req.session.store = null;
    }

    await res.json(data);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
