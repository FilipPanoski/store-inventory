const session = require('express-session');

module.exports = session({
  cookie: { maxAge: parseInt(process.env.SESSION_COOKIE_MAX_AGE, 10) },
  name: process.env.SESSION_NAME,
  resave: false,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
});
