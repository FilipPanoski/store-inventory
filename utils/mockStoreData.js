import { ERROR_FIX, ERROR_ON_LOCATION, SCAN_OK } from './inventoryItemStatuses';

export const dialog = {
  dialogProps: {
    eanImei: { value: '354422063048029' },
    error: true,
    inventoryItemType: 'SERIALIZED',
    key: 176,
    location: { value: 'Returned pending decision' },
    name: { value: 'iPad mini 3 16 GB Gold' },
    productId: { value: 'O01345' },
    quantity: { value: '0 (1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1)' },
    statusType: 'ERROR_ON_LOCATION',
  },
  dialogType: '',
  isDialogOpen: false,
};

export const inventoryItems = {
  list: [
    {
      ean: '7350080711048',
      historyOfScannedQuantities: '(1)',
      id: 'A02042',
      inventoryItemType: 'UNKNOWN',
      inventoryLocation: {
        id: 12,
        locationName: 'Sellable',
        locationNumber: '448',
      },
      lastScanDateTime: '2019-01-10 12:40:07',
      sessionId: 4,
      status: 'ERROR_ON_LOCATION',
      totalQuantity: 1,
    },
    {
      historyOfScannedQuantities: '(1)',
      id: 149,
      imei: '354422063048029',
      inventoryItemType: 'UNKNOWN',
      inventoryLocation: {
        id: 13,
        locationName: 'Sellable',
        locationNumber: '448',
      },
      lastScanDateTime: '2019-01-10 12:40:07',
      scanItems: [
        {
          inventoryLocation: {
            id: 8,
            locationName: 'Demo',
            locationNumber: '443',
          },
          quantity: 1,
          scanDateTime: '2019-01-15 12:57:05',
          scanItemEvent: ERROR_FIX,
          status: ERROR_ON_LOCATION,
        },
        {
          inventoryLocation: {
            id: 9,
            locationName: 'Loan',
            locationNumber: '444',
          },
          quantity: 1,
          scanDateTime: '2019-01-15 12:57:05',
          scanItemEvent: 'NEW_SCAN',
          status: SCAN_OK,
        },
      ],
      sessionId: 4,
      status: 'ITEM_NOT_FOUND',
      totalQuantity: 1,
    },
  ],
};

export const inventoryLocations = {
  list: [
    { id: 1, locationName: 'Demo', locationNumber: 443 },
    { id: 2, locationName: 'Loan', locationNumber: 444 },
  ],
};
