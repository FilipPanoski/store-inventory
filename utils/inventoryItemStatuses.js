export const SCAN_OK = 'SCAN_OK';
export const ERROR_ON_LOCATION = 'ERROR_ON_LOCATION';
export const ITEM_NOT_FOUND = 'ITEM_NOT_FOUND';
export const NOT_SCANNED = 'NOT_SCANNED';
export const ERROR_FIX = 'ERROR_FIX';
