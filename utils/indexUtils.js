import React from 'react';
import { NON_SERIALIZED } from './inventoryItemTypes';
import {
  ERROR_ON_LOCATION, ITEM_NOT_FOUND, NOT_SCANNED, SCAN_OK,
} from './inventoryItemStatuses';
import DescriptiveNotification from '../components/styles/Notification/DescriptiveNotification';

const columns = [
  {
    key: 'status',
  },
  {
    key: 'productId',
  },
  {
    key: 'eanImei',
  },
  {
    key: 'name',
  },
  {
    key: 'location',
  },
  {
    key: 'quantity',
  },
];

const getLastErrorLocation = (item) => {
  if (item.lastScannedLocation) {
    return item.lastScannedLocation;
  }
  const lastErrorLocation = item.scanItems.filter(scan => scan.status === ERROR_ON_LOCATION)[0];
  return lastErrorLocation ? lastErrorLocation.inventoryLocation : null;
};

const generateData = inventoryItems => inventoryItems.list.map(item => ({
  eanImei: { value: item.inventoryItemType === NON_SERIALIZED ? (item.ean) : (item.imei) },
  error: item.status !== NOT_SCANNED && item.status !== SCAN_OK,
  inventoryItemType: item.inventoryItemType,
  key: item.id,
  lastErrorLocation: getLastErrorLocation(item),
  location: { location: item.inventoryLocation, value: item.inventoryLocation.locationName },
  name: { value: item.productName },
  productId: { value: item.productId },
  quantity: { quantity: item.totalQuantity, value: `${item.totalQuantity} ${item.historyOfScannedQuantities}` },
  status: {
    value: <DescriptiveNotification
      success={item.status === SCAN_OK}
      error={item.status === ERROR_ON_LOCATION
        || item.status === ITEM_NOT_FOUND}
    />,
  },
  statusType: item.status,
}));

const generateNotFoundErrorItem = item => (
  {
    id: item.key,
    itemId: item.eanImei.value,
    location: item.location.location,
    quantity: item.quantity.quantity,
  }
);

const generateLocationErrorItem = item => (
  {
    errorLocation: item.lastErrorLocation,
    id: item.key,
    imei: item.eanImei.value,
    location: item.location.location,
    productId: item.productId.value,
    productName: item.name.value,
    quantity: item.quantity.quantity,
  }
);

const hasErrorsInItems = inventoryItems => inventoryItems.list.filter(
  item => item.status === ERROR_ON_LOCATION || item.status === ITEM_NOT_FOUND,
).length > 0;

const IndexUtils = {
  columns,
  generateData,
  generateLocationErrorItem,
  generateNotFoundErrorItem,
  hasErrorsInItems,
};

export default IndexUtils;
