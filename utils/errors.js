export const GENERAL_ERROR = { defaultMessage: 'Error occurred', status: 999 };
export const CIA_UNAVAILABLE = {
  defaultMessage: 'Third party services are not available',
  status: 1000,
};
export const USER_NOT_FOUND = { defaultMessage: 'User details does not exist', status: 1001 };
export const HEADER_MISSING_IN_REQUEST = {
  defaultMessage: 'User details does not exist',
  status: 1002,
};
export const OSCAR_UNAVAILABLE = { defaultMessage: 'Oscar system is not available', status: 2000 };
export const NO_RESULTS_FROM_OSCAR = {
  defaultMessage: 'Oscar system did not return any result',
  status: 2001,
};
export const ERP_UNAVAILABLE = { defaultMessage: 'ERP is not available', status: 3000 };
export const ERROR_GETTING_RESULTS = {
  defaultMessage: 'System did not return any result',
  status: 3001,
};
export const DB_ERROR = { defaultMessage: 'Database error occurred', status: 5000 };
