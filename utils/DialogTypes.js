/**
 * Values need to be the relative path from the DialogContainer to
 * the corresponding dialog component.
 */
export default {
  LOCATION_ERROR_DIALOG: './ConfirmLocationDialog',
  NEW_ITEM_DIALOG: './NewItemDialog',
  RESTART_DIALOG: './RestartActionModal',
  SAVE_DIALOG: './SaveActionModal',
  SUBMIT_DIALOG: './SubmitActionModal',
};
