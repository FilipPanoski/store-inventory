const isAlphaNumeric = input => /^[a-z0-9]*$/i.test(input);

export default isAlphaNumeric;
