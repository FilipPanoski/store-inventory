import React from 'react';
import { FormattedMessage } from 'react-intl';

export const isRequired = (value) => {
  if (!value.toString().trim().length) {
    return <ValidationError errorType="isRequired" />;
  }
  return null;
};

export const isQuantity = (value) => {
  if (!(value > 0 && value < 201)) {
    return <ValidationError errorType="isQuantity" />;
  }
  return null;
};

export const isImei = (value) => {
  if (value.toString().trim() && !(value.length > 6 && value.length < 31)) {
    return <ValidationError errorType="isImei" />;
  }
  return null;
};

export const isEan = (value) => {
  if (value.toString().trim() && !(value.length > 10 && value.length < 14)) {
    return <ValidationError errorType="isEan" />;
  }
  return null;
};

const ValidationError = ({ errorType }) => (
  <div className="validation-error">
    <FormattedMessage id={`validations_${errorType}`} />
  </div>
);
