const generateNewItem = ({
  productName, productId, location, imei, ean, status,
  scanDateTime, session, quantity, isSerialized, totalQuantity,
}) => ({
  attribute10SNFlag: isSerialized,
  ean,
  imei,
  inventoryLocation: location,
  productId,
  productName,
  scanItems: [
    {
      inventoryLocation: {
        id: location.id,
      },
      quantity,
      scanDateTime,
      status,
    },
  ],
  sessionId: session.id,
  totalQuantity,
});

export const editNewItem = ({
  id, productName, productId, location, imei, ean, status, scanDateTime,
  session, quantity, totalQuantity, isSerialized, scanItemEvent,
}) => ({
  attribute10SNFlag: isSerialized,
  ean,
  id,
  imei,
  inventoryLocation: location,
  productId,
  productName,
  scanItems: [
    {
      inventoryLocation: {
        id: location.id,
      },
      quantity,
      scanDateTime,
      scanItemEvent,
      status,
      totalQuantity,
    },
  ],
  sessionId: session.id,
});

export const putNewItem = ({
  id, imei, itemQuantity, location, productId, productName,
  quantity, scanDateTime, scanItemEvent, sessionId, status,
}) => ({
  attribute10SNFlag: '1',
  ean: null,
  id,
  imei,
  inventoryLocation: location,
  method: 'PUT',
  productId,
  productName,
  scanItems: [
    {
      inventoryLocation: location,
      quantity,
      scanDateTime,
      scanItemEvent,
      status,
      totalQuantity: itemQuantity,
    },
  ],
  sessionId,
});

export default generateNewItem;
