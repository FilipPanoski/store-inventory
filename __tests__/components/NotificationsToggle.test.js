import { shallow } from 'enzyme';
import React from 'react';

import NotificationsToggle from '../../components/NotificationsToggle';

const props = {
  toggleChange: jest.fn(),
};

let wrapper;

beforeEach(() => {
  wrapper = shallow(<NotificationsToggle {...props} />);
});

describe('<NotificationsToggle />', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
