import { shallow } from 'enzyme';
import React from 'react';

import QuantityPicker from '../../components/QuantityPicker';

const props = {
  handleDropdownKeyDown: jest.fn(),
  quantity: 1,
  saveQuantity: jest.fn(),
};

let wrapper;

beforeEach(() => {
  wrapper = shallow(<QuantityPicker {...props} />);
});

describe('<QuantityPicker />', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should save quantity on change', () => {
    const select = wrapper.find('.filter-options');
    select.simulate('change', { target: { value: 'Test' } });

    expect(props.saveQuantity).toHaveBeenCalled();
  });

  it('should handle key down', () => {
    const select = wrapper.find('.filter-options');
    select.simulate('keyDown', { key: 'a' });

    expect(props.handleDropdownKeyDown).toHaveBeenCalled();
  });
});
