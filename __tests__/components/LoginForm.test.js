import { shallow } from 'enzyme';
import React from 'react';
import configureStore from 'redux-mock-store';

// Testing the component with the all redux functionality.
import LoginFormRedux from '../../components/LoginForm'; // eslint-disable-line
// Testing the component purely without any redux.
import { LoginForm } from '../../components/LoginForm'; // eslint-disable-line

import { StoreSessionActions } from '../../store/actions/storeSession';
import Loader from '../../components/Loader';

const props = {
  cookie: { id: 123 },
  storeSession: {
    error: null, id: 123, isLoading: false, name: 'Store',
  },
  user: { entity: { loaded: false, loading: false, name: 'Jon Snow' } },
};

const initialState = {
  storeSession: { error: null, id: 123, name: 'Store' },
};

let wrapper;
let store;

const mockStore = configureStore();
const onStartSession = jest.fn();

describe('LoginForm', () => {
  describe('Component tests', () => {
    beforeEach(() => {
      wrapper = shallow(<LoginForm {...props} onStartSession={onStartSession} />);
    });

    afterEach(() => {
      onStartSession.mockClear();
    });

    it('should save to state', () => {
      const instance = wrapper.instance();
      const value = 123;

      jest.spyOn(instance, 'saveToState');
      instance.saveToState(value);

      expect(instance.state.store).toEqual(value);
    });

    it('should call an action when starting a session', () => {
      const instance = wrapper.instance();

      jest.spyOn(instance, 'startSession');
      instance.startSession({ preventDefault: () => { } });

      expect(onStartSession).toHaveBeenCalledTimes(1);
    });

    it('should render when loading', () => {
      props.storeSession.isLoading = true;
      wrapper = shallow(<LoginForm {...props} onStartSession={onStartSession} />);
      const instance = wrapper.instance();
      const fakeEvent = { preventDefault: () => '' };
      instance.startSession(fakeEvent);

      expect(wrapper.find(Loader).exists()).toBeTruthy();
    });

    it('should not render before session start', () => {
      props.storeSession.isLoading = false;
      wrapper = shallow(<LoginForm {...props} onStartSession={onStartSession} />);
      expect(wrapper.find(Loader).exists()).toBeFalsy();
    });

    // it('should render', () => {
    //   expect(wrapper).toMatchSnapshot();
    // });
  });

  describe('Redux tests', () => {
    beforeEach(() => {
      store = mockStore(initialState);
      wrapper = shallow(<LoginFormRedux store={store} user={props.user} cookie={props.cookie} />);
    });

    it('should get props from redux state', () => {
      expect(wrapper.prop('storeSession')).toEqual(initialState.storeSession);
    });

    it('should match redux actions', () => {
      const instance = wrapper.dive().instance();

      const startSessionAction = instance.props.onStartSession();

      expect(startSessionAction.type).toEqual(StoreSessionActions.START_SESSION);
    });
  });
});
