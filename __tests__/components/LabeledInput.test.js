import { shallow } from 'enzyme';
import React from 'react';

import LabeledInput from '../../components/LabeledInput';

const props = {
  disabled: false,
  id: 'labeled-input',
  label: 'Label',
  name: 'labeled-input',
  placeHolder: 'Placeholder',
  type: 'text',
  value: 'Value',
};
const onChange = jest.fn();

let wrapper;

beforeEach(() => {
  wrapper = shallow(<LabeledInput {...props} onChange={onChange} />);
});

describe('<LabeledInput />', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
