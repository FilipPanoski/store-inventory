import { shallow } from 'enzyme';
import React from 'react';

import Label from '../../components/Label';

const props = {
  component: {},
  id: 'labelId',
  name: 'label',
};

let wrapper;

beforeEach(() => {
  wrapper = shallow(<Label {...props} />);
});

describe('Label', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
