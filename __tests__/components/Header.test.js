import { shallow } from 'enzyme';
import React from 'react';

import Header from '../../components/Header';
import { fakeUser } from '../../utils/testUtils';

describe('<Header />', () => {
  it('should display the username', () => {
    const wrapper = shallow(<Header user={fakeUser} />);

    expect(wrapper.find('.user-name').text()).toBe(fakeUser.name);
  });

  it('should not display the user if no user is passed in the props', () => {
    const wrapper = shallow(<Header user={null} />);

    expect(wrapper.exists('.user-name')).toBeFalsy();
  });
});
