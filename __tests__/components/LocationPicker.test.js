import { shallow } from 'enzyme';
import React from 'react';

import LocationPicker from '../../components/LocationPicker';

const props = {
  location: 444,
  locations: [
    { id: 1, locationName: 'Demo', locationNumber: 448 },
    { id: 2, locationName: 'Loan', locationNumber: 444 },
  ],
  onChange: jest.fn(),
  onKeyDown: jest.fn(),
};

let wrapper;

beforeEach(() => {
  wrapper = shallow(<LocationPicker {...props} />);
});

describe('<LocationPicker />', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should not render label by default', () => {
    const labelWrapper = wrapper.find('.label-wrapper');

    expect(labelWrapper.exists()).toBeFalsy();
  });

  it('should render label when withLabel props is supplied', () => {
    wrapper = shallow(<LocationPicker {...props} withLabel />);

    const labelWrapper = wrapper.find('.label-wrapper');

    expect(labelWrapper.exists()).toBeTruthy();
  });
});
