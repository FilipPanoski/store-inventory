import { shallow } from 'enzyme';
import React from 'react';

import SelectedItem from '../../components/SelectedItem';
import Label from '../../components/Label';

const props = {
  id: 'selectedItem',
  isSerialized: true,
  name: 'selected-item',
  productId: 'F21456',
  productName: 'iPhone XS',
};

const deselectItem = jest.fn();

describe('SelectedItem', () => {
  it('should deselect an item when discard product icon is clicked', () => {
    const wrapper = shallow(<SelectedItem {...props} deselectItem={deselectItem} />);

    wrapper.find(Label).dive().find('.discard-product').simulate('click');

    expect(deselectItem).toHaveBeenCalledTimes(1);
  });

  it('should not display the discard icon when an existing item is received from parent', () => {
    const wrapper = shallow(<SelectedItem {...props} foundItem deselectItem={deselectItem} />);

    expect(wrapper.find(Label).dive().find('.discard-product').exists()).toEqual(false);
  });
});
