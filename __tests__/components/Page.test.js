import { shallow } from 'enzyme';
import React from 'react';
import configureStore from 'redux-mock-store';

import Page from '../../components/Page';

const initialState = {
  user: {
    entity: null,
  },
};
const mockStore = configureStore();
let wrapper;
let store;

beforeEach(() => {
  store = mockStore(initialState);
  wrapper = shallow(<Page store={store} />);
});

describe('Page', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
