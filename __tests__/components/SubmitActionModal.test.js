import { shallow } from 'enzyme';
import React from 'react';

import { SubmitActionModal } from '../../components/SubmitActionModal';
import Button from '../../components/styles/Button/Button';
import { ErrorMessage } from '../../components/styles/ErrorMessage';
import { ERROR_SUBMIT } from '../../utils/submitSessionStatus';

const props = {
  displayError: false,
  isLoading: false,
  onErpSubmit: jest.fn(),
  onToggleDialog: jest.fn(),
  successfulSubmit: false,
};

let wrapper;

beforeEach(() => {
  wrapper = shallow(<SubmitActionModal {...props} />);
});

describe('<SubmitActionModal />', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should submit to erp on submit button click', () => {
    const button = wrapper.find(Button).first();
    button.simulate('click');

    expect(props.onErpSubmit).toBeCalled();
  });

  it('should close dialog on cancel button click', () => {
    const button = wrapper.find(Button).at(1);
    button.simulate('click');

    expect(props.onToggleDialog).toBeCalled();
  });

  it('should render error when erpStatus returns an error', () => {
    wrapper = shallow(<SubmitActionModal {...props} erpStatus={ERROR_SUBMIT} />);
    const error = wrapper.find(ErrorMessage);

    expect(error.exists()).toBeTruthy();
  });
});
