import { shallow } from 'enzyme';
import React from 'react';

import DialogContainer from '../../components/DialogContainer';
import DialogTypes from '../../utils/DialogTypes';

let wrapper;

describe('DialogContainer', () => {
  it('should render NewItemDialog', () => {
    wrapper = shallow(<DialogContainer type={DialogTypes.NEW_ITEM_DIALOG} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render SaveDialog', () => {
    wrapper = shallow(<DialogContainer type={DialogTypes.SAVE_DIALOG} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render SubmitDialog', () => {
    wrapper = shallow(<DialogContainer type={DialogTypes.SUBMIT_DIALOG} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render RestartDialog', () => {
    wrapper = shallow(<DialogContainer type={DialogTypes.RESTART_DIALOG} />);
    expect(wrapper).toMatchSnapshot();
  });
});
