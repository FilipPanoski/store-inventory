import { shallow } from 'enzyme';
import React from 'react';

import Form from 'react-validation/build/form';
import ValidatedButton from 'react-validation/build/button';
import NewItemForm from '../../components/NewItemForm';
import Button from '../../components/styles/Button/Button';

const props = {
  ean: '',
  imei: '129156',
  isSerialized: true,
  location: { id: 1, name: 'Store' },
  name: 'iPhone',
  productId: '7FA996',
  quantity: 1,
};
const locations = [{ id: 1, name: 'Store' }, { id: 2, name: 'Demo' }];
const saveToState = jest.fn();
const saveNewItem = jest.fn();
const editNewItem = jest.fn();
const toggleNewItemDialog = jest.fn();

let wrapper;

beforeEach(() => {
  wrapper = shallow(<NewItemForm
    {...props}
    locations={locations}
    saveToState={saveToState}
    saveNewItem={saveNewItem}
    editNewItem={editNewItem}
    toggleNewItemDialog={toggleNewItemDialog}
  />);
});

afterEach(() => {
  saveNewItem.mockClear();
  editNewItem.mockClear();
});

describe('<NewItemForm />', () => {
  it('should save new item if the item is new', () => {
    wrapper.find(ValidatedButton).simulate('click', { preventDefault: () => { } });
    expect(saveNewItem.mock.calls).toHaveLength(1);
  });

  it('should edit new item if the item exists', () => {
    wrapper = shallow(<NewItemForm
      {...props}
      locations={locations}
      item
      saveToState={saveToState}
      saveNewItem={saveNewItem}
      editNewItem={editNewItem}
      toggleNewItemDialog={toggleNewItemDialog}
    />);

    wrapper.find(ValidatedButton).simulate('click', { preventDefault: () => { } });
    expect(editNewItem.mock.calls).toHaveLength(1);
  });

  it('should be able to toggle the dialog', () => {
    wrapper.find(Button).simulate('click', { preventDefault: () => { } });
    expect(toggleNewItemDialog.mock.calls).toHaveLength(1);
  });

  it('should preventDefault when Form is submitted', () => {
    const preventDefault = jest.fn();
    const mockEvent = { preventDefault };

    wrapper.find(Form).simulate('submit', mockEvent);

    expect(preventDefault.mock.calls).toHaveLength(1);
  });

  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
