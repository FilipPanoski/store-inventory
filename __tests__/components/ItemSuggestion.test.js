import { shallow } from 'enzyme';
import React from 'react';

import ItemSuggestion from '../../components/ItemSuggestion';
import ItemSuggestionStyle from '../../components/styles/Outputs/ItemSuggestionStyle';

const suggestions = [
  { productId: 'AF4586', productName: 'iPhone 6' },
  { productId: 'B899FA', productName: 'Redmi Note 5' },
];

const onSelectItem = jest.fn();

let wrapper;

beforeEach(() => {
  wrapper = shallow(<ItemSuggestion suggestions={suggestions} onSelectItem={onSelectItem} />);
});

describe('<ItemSuggestion />', () => {
  it('should display products from suggestions prop', () => {
    expect(wrapper.find('.suggestion-id')).toHaveLength(2);

    expect(wrapper.find('.suggestion-id').at(0).text()).toEqual(suggestions[0].productId);
    expect(wrapper.find('.suggestion-name').at(0).text()).toEqual(suggestions[0].productName);

    expect(wrapper.find('.suggestion-id').at(1).text()).toEqual(suggestions[1].productId);
    expect(wrapper.find('.suggestion-name').at(1).text()).toEqual(suggestions[1].productName);
  });

  it('should call a function when a suggestion is clicked', () => {
    wrapper.find(ItemSuggestionStyle).at(0).simulate('click');
    expect(onSelectItem).toHaveBeenCalledWith(suggestions[0]);
    expect(onSelectItem.mock.calls).toHaveLength(1);
  });

  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
