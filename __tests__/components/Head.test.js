import { shallow } from 'enzyme';
import React from 'react';

import Head from '../../components/Head';

describe('<Head />', () => {
  it('renders meta information for the app', () => {
    const title = 'Tre Stock Inventory';
    const description = 'React app for doing inventory in the shops';
    const wrapper = shallow(<Head title={title} description={description} />);

    expect(wrapper.find('title').text()).toEqual(title);
    expect(wrapper.find('meta[name="description"]').prop('content')).toEqual(description);
  });
});
