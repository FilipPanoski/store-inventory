import { shallow } from 'enzyme';
import React from 'react';
import configureStore from 'redux-mock-store';

// Testing the component purely without any redux.
import { NewItemDialog } from '../../components/NewItemDialog'; // eslint-disable-line
// Testing the component with the all redux functionality.
import NewItemDialogRedux from '../../components/NewItemDialog'; // eslint-disable-line

import { Button } from '../../components/styles/Button';
import { ProductCatalogueActions } from '../../store/actions/productCatalogue';
import { DialogActions } from '../../store/actions/dialog';
import { InventoryItemsActions } from '../../store/actions/inventoryItems';

const locations = [
  { id: 1, locationName: 'Demo', locationNumber: 443 },
  { id: 2, locationName: 'Loan', locationNumber: 444 },
];

const productCatalogue = [
  {
    ean: '5464656', imei: '5310071000287', productId: 'F21292', productName: 'iPhone X', serializedProduct: true,
  },
  {
    ean: '5464656', imei: null, productId: 'F21293', productName: 'HDMI Port', serializedProduct: false,
  },
];

const props = {
  locations,
  productCatalogue,
  storeSession: { sessionId: 123 },
};

const initialState = {
  inventoryLocations: { list: locations },
  productCatalogue: { productCatalogue },
  storeSession: { sessionId: 123 },
};

let wrapper;
let store;

const mockStore = configureStore();
const onEditNewItem = jest.fn();
const onGetProductCatalogue = jest.fn();
const onSaveNewItem = jest.fn();
const onToggleDialog = jest.fn();

describe('NewItemDialog', () => {
  describe('Component tests', () => {
    beforeEach(() => {
      wrapper = shallow(
        <NewItemDialog
          {...props}
          dialogProps={{ id: 1, location: 1 }}
          onEditNewItem={onEditNewItem}
          onGetProductCatalogue={onGetProductCatalogue}
          onSaveNewItem={onSaveNewItem}
          onToggleDialog={onToggleDialog}
        />,
      );
    });

    afterEach(() => {
      onGetProductCatalogue.mockClear();
      onSaveNewItem.mockClear();
      onToggleDialog.mockClear();
    });

    it('should update state when an item is selected', () => {
      const selectedItem = productCatalogue[0];
      const instance = wrapper.instance();

      jest.spyOn(instance, 'onSelectItem');
      instance.onSelectItem(selectedItem);

      expect(instance.onSelectItem).toHaveBeenCalledWith(selectedItem);
      expect(instance.state.ean).toEqual(productCatalogue[0].ean);
      expect(instance.state.imei).toEqual(productCatalogue[0].imei);
      expect(instance.state.isSerialized).toEqual(productCatalogue[0].serializedProduct);
      expect(instance.state.name).toEqual(productCatalogue[0].productName);
      expect(instance.state.productID).toEqual(productCatalogue[0].productId);
      expect(instance.state.isItemSelected).toEqual(true);
    });

    it('should update state when an item is deselected', () => {
      const instance = wrapper.instance();

      instance.deselectItem();

      expect(instance.state.isItemSelected).toEqual(false);
    });

    it('should update state when an input prop changes', () => {
      const instance = wrapper.instance();
      const value = 'Huawei';

      jest.spyOn(instance, 'saveToState');
      instance.saveToState({ target: { name: 'name', value } });

      expect(instance.state.name).toEqual(value);
    });

    it('should call actions when a new item is saved', () => {
      const instance = wrapper.instance();

      jest.spyOn(instance, 'saveNewItem');
      instance.saveNewItem();

      expect(onToggleDialog).toHaveBeenCalledTimes(1);
      expect(onSaveNewItem).toHaveBeenCalledTimes(1);
    });

    it('should call actions when a new item is edited', () => {
      const instance = wrapper.instance();

      jest.spyOn(instance, 'editNewItem');
      instance.editNewItem();

      expect(onToggleDialog).toHaveBeenCalledTimes(1);
      expect(onEditNewItem).toHaveBeenCalledTimes(1);
    });

    it('should toggle dialog when cancel is clicked', () => {
      wrapper.find(Button).simulate('click');

      expect(onToggleDialog).toHaveBeenCalledTimes(1);
    });
  });

  describe('Redux tests', () => {
    beforeEach(() => {
      store = mockStore(initialState);
      wrapper = shallow(<NewItemDialogRedux store={store} />);
    });

    it('should get props from redux state', () => {
      expect(wrapper.prop('locations')).toEqual(initialState.inventoryLocations.list);
      expect(wrapper.prop('productCatalogue')).toEqual(initialState.productCatalogue.productCatalogue);
      expect(wrapper.prop('storeSession')).toEqual(initialState.storeSession);
    });

    it('should match redux actions', () => {
      const instance = wrapper.dive().instance();

      const onEditNewItemAction = instance.props.onEditNewItem();
      const getProductCatalogueAction = instance.props.onGetProductCatalogue();
      const toggleDialogAction = instance.props.onToggleDialog();
      const saveNewItemAction = instance.props.onSaveNewItem();

      expect(onEditNewItemAction.type).toEqual(InventoryItemsActions.NEW_ITEM_EDIT);
      expect(getProductCatalogueAction.type).toEqual(ProductCatalogueActions.GET_PRODUCT_CATALOGUE);
      expect(toggleDialogAction.type).toEqual(DialogActions.TOGGLE_DIALOG);
      expect(saveNewItemAction.type).toEqual(InventoryItemsActions.NEW_ITEM_SAVE);
    });
  });
});
