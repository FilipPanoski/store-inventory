import { shallow } from 'enzyme';
import React from 'react';

import RegisterToggle from '../../components/RegisterToggle';

const props = {
  toggleChange: jest.fn(),
};

let wrapper;

beforeEach(() => {
  wrapper = shallow(<RegisterToggle {...props} />);
});

describe('<RegisterToggle />', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
