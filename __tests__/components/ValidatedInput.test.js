import { shallow } from 'enzyme';
import React from 'react';

import ValidatedInput from '../../components/ValidatedInput';

const props = {
  disabled: false,
  id: 'labeled-input',
  label: 'Label',
  maxLength: 15,
  name: 'labeled-input',
  placeHolder: 'Placeholder',
  type: 'text',
  validations: [],
  value: 'Value',
};
const onChange = jest.fn();

let wrapper;

beforeEach(() => {
  wrapper = shallow(<ValidatedInput {...props} onChange={onChange} />);
});

describe('<ValidatedInput />', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
