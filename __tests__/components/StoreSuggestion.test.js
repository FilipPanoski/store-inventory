import { shallow } from 'enzyme';
import React from 'react';
import * as sinon from 'sinon';

import StoreSuggestion from '../../components/StoreSuggestion';

const storeNumber = '1234';
const storeName = 'Sweden store';
const suggestedStores = { [storeNumber]: storeName };

const onChange = jest.fn();

describe('<StoreSuggestion />', () => {
  describe('input field', () => {
    it('should be able to input a number', () => {
      const wrapper = shallow(
        <StoreSuggestion onChange={onChange} id="storeNumber" suggestedStores={suggestedStores} />,
      );

      wrapper.find('input').simulate('change', { target: { value: storeNumber } });

      expect(wrapper.state('storeNumber')).toEqual(storeNumber);
      expect(wrapper.state('openSuggestion')).toBeFalsy();
    });
  });

  describe('suggestion dialog', () => {
    it('should be closed by default', () => {
      const wrapper = shallow(<StoreSuggestion onChange={onChange} id="storeNumber" suggestedStores={suggestedStores} />);

      expect(wrapper.exists('.suggestion')).toBeFalsy();
    });

    it('should be opened when input is focused', () => {
      const wrapper = shallow(
        <StoreSuggestion onChange={onChange} id="storeNumber" suggestedStores={suggestedStores} />,
      );

      wrapper.find('input').prop('onFocus')();

      expect(wrapper.exists('.suggestion')).toBeTruthy();
    });

    it('should not be open if the suggestedStore prop is not provided', () => {
      const wrapper = shallow(<StoreSuggestion onChange={onChange} id="storeNumber" />);

      wrapper.find('input').prop('onFocus')();

      expect(wrapper.exists('.suggestion')).toBeFalsy();
    });

    it('should display the suggestedStore', () => {
      const wrapper = shallow(
        <StoreSuggestion onChange={onChange} id="storeNumber" suggestedStores={suggestedStores} />,
      );

      wrapper.find('input').prop('onFocus')();

      expect(wrapper.find('.suggestion__item').text()).toEqual(storeNumber);
    });

    it('should set the input value to the selected suggestion', () => {
      const wrapper = shallow(
        <StoreSuggestion onChange={onChange} id="storeNumber" suggestedStores={suggestedStores} />,
      );
      const spy = sinon.spy(wrapper.instance(), 'useSuggestion');

      wrapper.update();

      wrapper.find('input').prop('onFocus')();
      wrapper.find('.suggestion__item').simulate('click');

      expect(spy.calledOnce).toBeTruthy();
      expect(wrapper.state().storeNumber).toEqual(storeNumber);
    });
  });
});
