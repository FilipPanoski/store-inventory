import { shallow } from 'enzyme';
import React from 'react';
import configureMockStore from 'redux-mock-store';

import NewItemDialogWrapper from '../../components/NewItemDialogWrapper';
import DialogTypes from '../../utils/DialogTypes';
import { DialogActions } from '../../store/actions/dialog';

const props = {
  onToggleDialog: jest.fn(),
};

let wrapper;
let store;
const mockStore = configureMockStore();

beforeEach(() => {
  const initialState = {
    dialogType: '',
    isDialogOpen: false,
  };

  store = mockStore(initialState);
  wrapper = shallow(<NewItemDialogWrapper store={store} {...props} />);
});

describe('<NewItemDialogWrapper />', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should toggle new item dialog on key press', () => {
    const span = wrapper.dive().find('.new-item');
    span.simulate('keyPress', { key: 'a' });

    const actions = store.getActions();
    expect(actions).toEqual([{
      payload: { props: undefined, type: DialogTypes.NEW_ITEM_DIALOG },
      type: DialogActions.TOGGLE_DIALOG,
    }]);
  });

  it('should toggle dialog on click', () => {
    const span = wrapper.dive().find('.new-item');
    span.simulate('click');

    const actions = store.getActions();
    expect(actions).toEqual([{
      payload: { props: undefined, type: DialogTypes.NEW_ITEM_DIALOG },
      type: DialogActions.TOGGLE_DIALOG,
    }]);
  });
});
