import { shallow } from 'enzyme';
import React from 'react';

import Search from '../../components/Search';
import ItemSuggestion from '../../components/ItemSuggestion';
import ItemSuggestionStyle from '../../components/styles/Outputs/ItemSuggestionStyle';

const items = [
  { productId: 'F21B94', productName: 'iPhone 6' },
  { productId: 'F21F99', productName: 'Xiaomi Light' },
];
const deselectItem = jest.fn();
const onSelectItem = jest.fn();

afterEach(() => {
  deselectItem.mockClear();
  onSelectItem.mockClear();
});

describe('<Search/>', () => {
  describe('when item is not selected', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = shallow(<Search
        isItemSelected={false}
        onSelectItem={onSelectItem}
        deselectItem={deselectItem}
        items={items}
      />);
    });

    it('should be able to search', () => {
      const value = 'F3396A';
      wrapper.find('#search').simulate('change', { target: { value } });
      expect(wrapper.find('#search').prop('value')).toEqual(value);
    });

    it('should be able to select an item', () => {
      const value = 'F21B94';
      wrapper.find('#search').simulate('change', { target: { value } });

      wrapper.find(ItemSuggestion).dive().find(ItemSuggestionStyle).simulate('click');
      expect(onSelectItem.mock.calls).toHaveLength(1);
      expect(wrapper.state('productId')).toEqual(items[0].productId);
      expect(wrapper.state('productName')).toEqual(items[0].productName);
    });

    it('should filter items when searching', () => {
      const value = 'F21';
      wrapper.find('#search').simulate('change', { target: { value } });
      expect(wrapper).toMatchSnapshot();

      wrapper.find('#search').simulate('change', { target: { value: `${value}B` } });
      expect(wrapper).toMatchSnapshot();
    });

    it('should render', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('when item is selected', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = shallow(<Search isItemSelected deselectItem={deselectItem} />);
    });

    it('should render', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('when an item is received from parent', () => {
    it('should select the item if it is found in items list', () => {
      const wrapper = shallow(
        <Search
          isItemSelected={false}
          onSelectItem={onSelectItem}
          deselectItem={deselectItem}
          items={items}
          item={items[0].productId}
        />,
      );

      const instance = wrapper.instance();
      jest.spyOn(instance, 'findItem');
      instance.findItem(instance.props.item);

      expect(onSelectItem).toHaveBeenCalled();
      expect(instance.state.hasError).toEqual(false);
    });

    it('should display error if it is not found in items list', () => {
      const wrapper = shallow(
        <Search
          isItemSelected={false}
          onSelectItem={onSelectItem}
          deselectItem={deselectItem}
          items={items}
          item="NonExistentItem"
        />,
      );

      const instance = wrapper.instance();
      jest.spyOn(instance, 'findItem');
      instance.findItem(instance.props.item);

      expect(onSelectItem).toHaveBeenCalledTimes(0);
      expect(instance.state.hasError).toEqual(true);
    });
  });
});
