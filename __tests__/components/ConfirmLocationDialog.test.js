import { shallow } from 'enzyme';
import React from 'react';
import configureStore from 'redux-mock-store';

// Testing the component purely without any redux.
import { ConfirmLocationDialog } from '../../components/ConfirmLocationDialog'; // eslint-disable-line
// Testing the component with the all redux functionality.
import ConfirmLocationDialogRedux from '../../components/ConfirmLocationDialog'; // eslint-disable-line

import { InventoryItemsActions } from '../../store/actions/inventoryItems';
import { DialogActions } from '../../store/actions/dialog';

const locations = [
  { id: 1, locationName: 'Demo', locationNumber: '443' },
  { id: 2, locationName: 'Loan', locationNumber: '444' },
];

const props = {
  dialogProps: {
    errorLocation: { id: 123, locationName: 'Sellable', locationNumber: 456 },
    id: 123456,
    imei: 12345678,
    location: { id: 456, locationName: 'Demo', locationNumber: 789 },
    productId: 'AF1235',
    productName: 'iPhone X',
    quantity: 1,
  },
  locations,
  onItemPut: jest.fn(),
  onToggleDialog: jest.fn(),
  storeSession: { sessionId: 45678 },
};

const initialState = {
  inventoryLocations: { list: locations },
  storeSession: { sessionId: 123 },
};

const mockStore = configureStore();

let wrapper;
let store;

describe('<ConfirmLocationDialog />', () => {
  describe('Component tests', () => {
    beforeEach(() => {
      wrapper = shallow(<ConfirmLocationDialog {...props} />);
    });

    it('should submit a request and toggle dialog on confirmation', () => {
      const instance = wrapper.instance();
      instance.confirmLocation();

      expect(props.onItemPut).toBeCalledTimes(1);
      expect(props.onToggleDialog).toBeCalledTimes(1);
    });

    it('should change state location', () => {
      const instance = wrapper.instance();
      const mockLocation = { id: 777, locationName: 'Mock Location', locationNumber: 999 };
      const mockEvent = { target: { value: mockLocation } };

      instance.setLocation(mockEvent);

      expect(instance.state.location).toEqual(mockLocation);
    });
  });

  describe('Redux tests', () => {
    beforeEach(() => {
      store = mockStore(initialState);
      wrapper = shallow(<ConfirmLocationDialogRedux {...props} store={store} />);
    });

    it('should get props from redux state', () => {
      expect(wrapper.prop('locations')).toEqual(initialState.inventoryLocations.list);
      expect(wrapper.prop('storeSession')).toEqual(initialState.storeSession);
    });

    it('should match redux actions', () => {
      const instance = wrapper.dive().instance();

      const onItemPut = instance.props.onItemPut();
      const onToggleDialog = instance.props.onToggleDialog();

      expect(onItemPut.type).toEqual(InventoryItemsActions.EXISTING_ITEM_PUT);
      expect(onToggleDialog.type).toEqual(DialogActions.TOGGLE_DIALOG);
    });
  });
});
