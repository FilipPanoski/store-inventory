import React from 'react';
import { shallow } from 'enzyme';

import TableList from '../../components/TableList';
import DescriptiveNotification from '../../components/styles/Notification/DescriptiveNotification';
import { NON_SERIALIZED, SERIALIZED } from '../../utils/inventoryItemTypes';

const columns = [
  {
    key: 'scanned',
    text: 'Scanned',
  },
  {
    key: 'productId',
    text: 'Product ID',
  },
  {
    key: 'name',
    text: 'Name',
  },
  {
    key: 'location',
    text: 'Location',
  },
  {
    key: 'type',
    text: 'Type',
  },
  {
    key: 'quantity',
    text: 'Quantity',
  },
];

const data = [
  {
    key: 1,
    location: { value: 'Store' },
    name: { value: 'iPhone' },
    productId: { value: 'A0123' },
    quantity: { value: 0 },
    scanned: { value: <DescriptiveNotification error /> },
    type: SERIALIZED,
  },
  {
    key: 2,
    location: { value: 'Demo' },
    name: { value: 'USB Cable' },
    productId: { value: 'A0456' },
    quantity: { value: 1 },
    scanned: { value: <DescriptiveNotification success /> },
    type: NON_SERIALIZED,
  },
];

let wrapper;

beforeEach(() => {
  wrapper = shallow(<TableList columns={columns} data={data} filterBy="inventoryItemType" />);
});

describe('<TableList/>', () => {
  describe('table', () => {
    it('should filter all items', () => {
      wrapper.find('.filter-options').simulate('change', { target: { value: '' } });
      expect(wrapper).toMatchSnapshot();
    });

    it('should filter serialized items', () => {
      wrapper.find('.filter-options').simulate('change', { target: { value: SERIALIZED } });

      expect(wrapper.state('dataFilter')).toEqual(SERIALIZED);
      expect(wrapper).toMatchSnapshot();
    });

    it('should filter non-serialized items', () => {
      wrapper.find('.filter-options').simulate('change', { target: { value: NON_SERIALIZED } });

      expect(wrapper.state('dataFilter')).toEqual(NON_SERIALIZED);
      expect(wrapper).toMatchSnapshot();
    });
  });
});
