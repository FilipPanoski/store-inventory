import { shallow } from 'enzyme';
import React from 'react';

import LocationDialogContent from '../../components/LocationDialogContent';
import Button from '../../components/styles/Button/Button';


const props = {
  location: '443',

  locations: [
    { id: 1, locationName: 'Demo', locationNumber: '443' },
    { id: 2, locationName: 'Loan', locationNumber: '444' },
  ],

  onConfirmLocation: jest.fn(),
  onToggleDialog: jest.fn(),
  setLocation: jest.fn(),
};

let wrapper;

beforeEach(() => {
  wrapper = shallow(<LocationDialogContent {...props} />);
});

describe('<LocationDialogInput />', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should confirm location on button submit', () => {
    const button = wrapper.find(Button).first();
    button.simulate('click');

    expect(props.onConfirmLocation).toBeCalled();
  });

  it('should toggle dialog on cancel', () => {
    const button = wrapper.find(Button).at(1);
    button.simulate('click');

    expect(props.onToggleDialog).toBeCalled();
  });
});
