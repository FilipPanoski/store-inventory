import { shallow, mount } from 'enzyme';
import React from 'react';
import configureStore from 'redux-mock-store';

import ItemIdInputWrapper from '../../components/ItemIdInputWrapper';

let wrapper;
let store;

const props = {
  itemID: '7350080711048',
  location: { id: 448, locationName: 'Demo', locationNumber: 448 },
  quantity: 1,
  scanAction: 'Register',
  setScannerId: jest.fn(),
};

const initialState = {
  inventoryItems: {
    list: [
      {
        ean: '7350080711048',
        historyOfScannedQuantities: '(1)',
        id: 'A02042',
        inventoryItemType: 'UNKNOWN',
        inventoryLocation: {
          id: 12,
          locationName: 'Sellable',
          locationNumber: '448',
        },
        lastScanDateTime: '2019-01-10 12:40:07',
        sessionId: 4,
        status: 'ERROR_ON_LOCATION',
        totalQuantity: 1,
      },
      {
        historyOfScannedQuantities: '(1)',
        id: 149,
        imei: '876876',
        inventoryItemType: 'UNKNOWN',
        inventoryLocation: {
          id: 13,
          locationName: 'Sellable',
          locationNumber: '448',
        },
        lastScanDateTime: '2019-01-10 12:40:07',
        sessionId: 4,
        status: 'ITEM_NOT_FOUND',
        totalQuantity: 1,
      },
    ],
  },
  inventoryLocations: {
    list:
      [
        { id: 1, locationName: 'Demo', locationNumber: 448 },
        { id: 2, locationName: 'Loan', locationNumber: 444 },
      ],
  },
  storeSession: {
    error: null,
    id: 4,
    name: '3Butikerna Region StorSthlm',
  },
};

const mockStore = configureStore();

beforeEach(() => {
  store = mockStore(initialState);
  store.clearActions();

  wrapper = shallow(<ItemIdInputWrapper store={store} {...props} />);
});

describe('<ItemIdInputWrapper />', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should bind keydown event when component mounts', () => {
    const instance = wrapper.dive().instance();
    const bindSpy = jest.spyOn(instance, 'bindScannerEvent');
    instance.componentDidMount();

    expect(bindSpy).toBeCalled();
  });

  it('should set text input on keydown event', () => {
    const map = {};
    document.addEventListener = jest.fn((event, cb) => {
      map[event] = cb;
    });

    mount(<ItemIdInputWrapper store={store} {...props} />);
    map.keydown({ key: 'a', target: { name: undefined } });

    expect(document.addEventListener).toHaveBeenCalled();
    expect(props.setScannerId).toHaveBeenCalled();
  });

  it('should not set text input if target is not a scanner', () => {
    const map = {};
    document.addEventListener = jest.fn((event, cb) => {
      map[event] = cb;
    });

    mount(<ItemIdInputWrapper store={store} {...props} />);
    map.keydown({ key: 'a', target: { name: 'notascanner' } });

    expect(document.addEventListener).toHaveBeenCalled();
    expect(props.setScannerId).toHaveBeenCalledTimes(1);
  });

  it('should only accept alpha-numeric values', () => {
    const map = {};
    document.addEventListener = jest.fn((event, cb) => {
      map[event] = cb;
    });

    mount(<ItemIdInputWrapper store={store} {...props} />);
    map.keydown({ key: 'Tab', target: { name: undefined } });

    expect(document.addEventListener).toHaveBeenCalled();
    expect(props.setScannerId).toHaveBeenCalledTimes(1);
  });

  it('should handle item found', () => {
    const instance = wrapper.dive().instance();
    const bindSpyFound = jest.spyOn(instance, 'handleItemFound');
    const bindSpyNotFound = jest.spyOn(instance, 'handleItemNotFound');
    instance.checkItem();

    expect(bindSpyFound).toHaveBeenCalled();
    expect(bindSpyNotFound).toHaveBeenCalledTimes(0);
  });

  it('should handle item not found', () => {
    wrapper = shallow(<ItemIdInputWrapper store={store} {...props} itemID="nosuchitem123" />);
    const instance = wrapper.dive().instance();

    const bindSpyFound = jest.spyOn(instance, 'handleItemFound');
    const bindSpyNotFound = jest.spyOn(instance, 'handleItemNotFound');
    instance.checkItem();

    expect(bindSpyNotFound).toHaveBeenCalled();
    expect(bindSpyFound).toHaveBeenCalledTimes(0);
  });
});
