const express = require('express');
const nextApp = require('next');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = nextApp({ dev });
const handle = app.getRequestHandler();

const routes = require('./routes');

app
  .prepare()
  .then(() => {
    const server = express();

    server.use(routes);

    server.get('/', (req, res, next) => {
      // Disable cache
      res.header(
        'Cache-Control',
        'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0',
      );

      const userId = req.query.userId ? req.query.userId : process.env.DEFAULT_USER;

      if (!req.session.store) {
        res.redirect(301, `/login?userId=${userId}`);
      } else {
        next();
      }
    });

    server.get('*', (req, res) => handle(req, res));

    server.listen(port, (err) => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${port}`);//eslint-disable-line
    });
  })
  .catch((ex) => {
    console.error(ex.stack);//eslint-disable-line
    process.exit(1);
  });
